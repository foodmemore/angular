import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideStore } from '@ngrx/store';
import { routes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';

import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  NativeDateAdapter,
} from '@angular/material/core';
import { MY_DATES_FORMATS } from './config/constants/date_format';
import {
  HTTP_INTERCEPTORS,
  provideHttpClient,
  withInterceptors,
} from '@angular/common/http';

import { provideEffects } from '@ngrx/effects';
import { UserReducer } from './core/user/store/user.reducer';
import { DishReducer } from './core/dish/store/dish.reducer';
import { UserEffect } from './core/user/store/user.effect';
import { authInterceptor } from './core/interceptor/auth/auth.interceptor';
import { AppInterceptor } from './core/interceptor/app/app.interceptor';
import { DishEffect } from './core/dish/store/dish.effect';
import { MenuReducer } from './core/menu/store/menu.reducter';
import { MenuEffect } from './core/menu/store/menu.effect';
import { CommandReducer } from './core/command/store/command.reducer';
import { LOCALE_ID } from '@angular/core';
import { provideNgxStripe } from 'ngx-stripe';
import { environment } from 'environments/environment';
import { CommandEffect } from './core/command/store/command.effect';
import { AllergenReducer } from './core/allergen/store/allergen.reducer';
import { AllergenEffect } from './core/allergen/store/allergen.effect';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideNgxStripe(environment.stipe_public_key),
    provideAnimationsAsync(),
    provideHttpClient(withInterceptors([authInterceptor])),
    { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
    { provide: DateAdapter, useClass: NativeDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATES_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    { provide: LOCALE_ID, useValue: 'fr' },
    provideStore({
      user: UserReducer,
      dish: DishReducer,
      menu: MenuReducer,
      command: CommandReducer,
      allergen: AllergenReducer,
    }),
    provideEffects([UserEffect, DishEffect, MenuEffect, CommandEffect, AllergenEffect]),
  ],
};
