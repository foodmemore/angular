import { AsyncPipe } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectCurrentUseRoleState } from 'app/core/user/store/user.selector';
import { UserCurrentUseRole } from 'app/core/user/store/user.state';
import { Observable } from 'rxjs';
import { NavLoadingComponent } from './child/nav-loading/nav-loading.component';
import { NavNotLoggedComponent } from './child/nav-not-logged/nav-not-logged.component';
import { NavUserLoggedComponent } from './child/nav-user-logged/nav-user-logged.component';
import { NavCookerLoggedComponent } from './child/nav-cooker-logged/nav-cooker-logged.component';
import { NavAdminLoggedComponent } from './child/nav-admin-logged/nav-admin-logged.component';
import { NavSuperAdminLoggedComponent } from './child/nav-super-admin-logged/nav-super-admin-logged.component';

@Component({
  selector: 'app-navbar',
  standalone: true,
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
  imports: [
    AsyncPipe,
    NavLoadingComponent,
    NavNotLoggedComponent,
    NavUserLoggedComponent,
    NavCookerLoggedComponent,
    NavAdminLoggedComponent,
    NavSuperAdminLoggedComponent,
  ],
})
export class NavbarComponent implements OnInit {
  currentUseRole$!: Observable<UserCurrentUseRole>;
  readonly store: Store = inject(Store);

  ngOnInit(): void {
    this.currentUseRole$ = this.store.select(selectCurrentUseRoleState);
  }

  useCurrentRoleEnum = UserCurrentUseRole;
}
