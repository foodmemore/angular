import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faLock, faCircleUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-nav-admin-logged',
  standalone: true,
  imports: [FontAwesomeModule, RouterOutlet, RouterModule],
  template: `
    <ul>
      <li routerLink="admin" [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faLock"></fa-icon>
        <span>Admin</span>
      </li>
      <li routerLink="profile" [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faCircleUser"></fa-icon>
        <span>Profile</span>
      </li>
      <ul></ul>
    </ul>
  `,
  styles: `
      @import "nav-mobile";
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavAdminLoggedComponent {
  faCircleUser = faCircleUser;
  faLock = faLock;
}
