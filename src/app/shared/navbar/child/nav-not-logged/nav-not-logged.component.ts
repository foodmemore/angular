import { ChangeDetectionStrategy, Component, inject } from '@angular/core';

import { RouterOutlet, RouterModule, Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faMessage,
  faCircleUser,
  faMagnifyingGlass,
  faBasketShopping,
} from '@fortawesome/free-solid-svg-icons';
import { LiCommendComponent } from "../../nav/child/li-commend/li-commend.component";

@Component({
    selector: 'app-nav-not-logged',
    standalone: true,
    template: `
    <ul>
      <li class="active" routerLink="menu" [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faMagnifyingGlass"></fa-icon>
        <span>Menu </span>
      </li>
      
      <app-li-commend />

      <li routerLink="connexion" [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faCircleUser"></fa-icon>
        <span>Connexion</span>
      </li>
    </ul>
  `,
    styles: `@import "nav-mobile"`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [FontAwesomeModule, RouterOutlet, RouterModule, LiCommendComponent]
})
export class NavNotLoggedComponent {
  faMessage = faMessage;
  faCircleUser = faCircleUser;
  faMagnifyingGlass = faMagnifyingGlass;
  faBasketShopping = faBasketShopping;


  readonly router: Router = inject(Router);
}
