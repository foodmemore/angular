import { ChangeDetectionStrategy, Component, inject } from '@angular/core';

import { RouterOutlet, RouterModule, Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faCircleUser,
  faMagnifyingGlass,
  faUtensils,
} from '@fortawesome/free-solid-svg-icons';
import { LiCommendComponent } from '../../nav/child/li-commend/li-commend.component';

@Component({
  selector: 'app-nav-user-logged',
  standalone: true,
  template: `
    <ul>
      <li
        class="active"
        routerLink="menu"
        [routerLinkActive]="['active', '']"
        role="listitem"
      >
        <fa-icon [icon]="faMagnifyingGlass"></fa-icon>
        <span>Menu</span>
      </li>

      <app-li-commend />



      <li
        role="listitem"
        routerLink="commande/utilisateur"
        [routerLinkActive]="['active', '']"
      >
        <fa-icon [icon]="faUtensils"></fa-icon>
        <span>Commande</span>
      </li>

      <li
        routerLink="profile"
        [routerLinkActive]="['active', '']"
        role="listitem"
      >
        <fa-icon [icon]="faCircleUser"></fa-icon>
        <span>Profile</span>
      </li>
    </ul>
  `,
  styles: `
    @import "nav-mobile";
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FontAwesomeModule, RouterOutlet, RouterModule, LiCommendComponent],
})
export class NavUserLoggedComponent {
  faMagnifyingGlass = faMagnifyingGlass;
  faUtensils = faUtensils;
  faCircleUser = faCircleUser;
  readonly router: Router = inject(Router);
}
