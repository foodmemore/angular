import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavLoadingComponent } from './nav-loading.component';

describe('NavLoadingComponent', () => {
  let component: NavLoadingComponent;
  let fixture: ComponentFixture<NavLoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NavLoadingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NavLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
