import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-nav-loading',
  standalone: true,
  imports: [],
  template: `
    <p>
      nav-loading works!
    </p>
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavLoadingComponent {

}
