import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { RouterOutlet, RouterModule, Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faKitchenSet,
  faCircleUser,
  faBasketShopping,
  faUtensilSpoon,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-nav-cooker-logged',
  standalone: true,

  imports: [FontAwesomeModule, RouterOutlet, RouterModule],
  template: `
    <ul>
      <li routerLink="preparation" [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faKitchenSet"></fa-icon>
        <span>Préparations</span>
      </li>
      <li routerLink="commande" [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faBasketShopping"></fa-icon>
        <span>Commande</span>
      </li>
      <!-- <li [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faUtensilSpoon"></fa-icon>
        <span>Commande</span>
      </li> -->
      <li routerLink="profile" [routerLinkActive]="['active', '']">
        <fa-icon [icon]="faCircleUser"></fa-icon>
        <span>Profile</span>
      </li>
    </ul>
  `,
  styles: `
    @import "nav-mobile";
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavCookerLoggedComponent {
  faKitchenSet = faKitchenSet;
  faUtensilSpoon = faUtensilSpoon;
  faCircleUser = faCircleUser;
  faBasketShopping = faBasketShopping;

  currentRoute!: string;
  readonly router: Router = inject(Router);
}
