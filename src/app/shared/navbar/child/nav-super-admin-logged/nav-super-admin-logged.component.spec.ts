import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavSuperAdminLoggedComponent } from './nav-super-admin-logged.component';

describe('NavSuperAdminLoggedComponent', () => {
  let component: NavSuperAdminLoggedComponent;
  let fixture: ComponentFixture<NavSuperAdminLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NavSuperAdminLoggedComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NavSuperAdminLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
