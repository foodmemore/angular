import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-nav-super-admin-logged',
  standalone: true,
  imports: [],
  template: `
    <p>
      nav-super-admin-logged works!
    </p>
  `,
  styles: `@import "nav-mobile"`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavSuperAdminLoggedComponent {

}
