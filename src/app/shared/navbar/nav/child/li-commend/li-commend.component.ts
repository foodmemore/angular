import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faBasketShopping } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { MatBadgeModule } from '@angular/material/badge';
import {
  selectCommandAnimation,
  selectCommandNumber,
} from 'app/core/command/store/command.selector';
import { AsyncPipe, JsonPipe } from '@angular/common';
import * as CommandActions from 'app/core/command/store/command.action';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-li-commend',
  standalone: true,
  imports: [
    FontAwesomeModule,
    RouterModule,
    MatBadgeModule,
    AsyncPipe,
    JsonPipe,
  ],
  template: `
    <li
      routerLink="panier"
      [routerLinkActive]="['active', '']"
      aria-label="Accéder au panier"
      role="listitem"
    >
      @if(commandsNumber$ | async; as commandsNumber) { @if(commandsNumber > 0)
      {
      <span class="badge sm">{{ commandsNumber }}</span>
      } } @if(commandState$ | async; as state) { @if(state.commands.length < 0)
      {
      <span class="badge sm">{{ state.commands.length }}</span>

      } @if(state.changeActive) {

      <fa-icon class="fa-shake" [icon]="faBasketShopping"></fa-icon>
      } @else {

      <fa-icon [icon]="faBasketShopping"></fa-icon>
      }
      <span>Panier</span>
      }
    </li>
  `,
  styles: `@import "nav-mobile";
  li {
    position: relative;
    .badge {
      position: absolute;
      z-index: 999;
      top: -10px;
      right: 10px;
      background-color: $secondBackgroundLight;
      font-weight: bold;
  
      padding: 0.4rem 0.3rem;
      border-radius: 100%;
    }
  }
  .fa-shake {
  -webkit-animation-duration: 2s;
  animation-duration: 2s;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;}
  `,

  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LiCommendComponent {
  faBasketShopping = faBasketShopping;
  readonly router: Router = inject(Router);
  readonly store: Store = inject(Store);
  commandsNumber$ = this.store.select(selectCommandNumber);
  commandState$ = this.store.select(selectCommandAnimation);
  private subscription: Subscription = new Subscription();



  putAnimationAtFalse() {
    setInterval(() => {
      this.store.dispatch(
        CommandActions.toggleAnimation({ changeActive: false })
      );
    }, 4000);
  }
}
