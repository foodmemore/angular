import { ChangeDetectionStrategy, Component, inject, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { getAllergen } from 'app/core/allergen/store/allergen.action';

@Component({
  selector: 'app-store-allergen',
  standalone: true,
  imports: [],
  template: '',
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class StoreAllergenSelectorComponent implements OnInit {
  readonly store = inject(Store);
  ngOnInit(): void {
    this.store.dispatch(getAllergen())

  
  }
}
