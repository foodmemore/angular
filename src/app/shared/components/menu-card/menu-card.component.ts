import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-menu-card',
  standalone: true,
  imports: [],
  template: `
    <p>
      menu-card works!
    </p>
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuCardComponent {

}
