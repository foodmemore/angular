import { ChangeDetectionStrategy, Component, OnInit, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { refreshTokens } from 'app/core/user/store/user.action';

@Component({
  selector: 'app-refresh-tokens',
  standalone: true,
  imports: [],
  template: `
 
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RefreshTokensComponent implements OnInit {

  readonly store: Store = inject(Store);

  ngOnInit(): void {
    this.refreshTokens();

    setInterval(() => {
      this.refreshTokens();
    }, 1000 * 60 * 10);
    
  }

  private refreshTokens() {
    console.info('refreshTokens');
    this.store.dispatch(refreshTokens());
}
}