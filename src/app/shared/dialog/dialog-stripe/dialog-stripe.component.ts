/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  Inject,
  inject,
  signal,
  ViewChild,
} from '@angular/core';
import {
  ReactiveFormsModule,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';

import { MatInputModule } from '@angular/material/input';

import {
  injectStripe,
  StripeElementsDirective,
  StripePaymentElementComponent,
} from 'ngx-stripe';
import {
  StripeElementsOptions,
  StripePaymentElementOptions,
} from '@stripe/stripe-js';
import { PaymentGatewayService } from 'app/core/payment/gateway/payment.gateway';
import { environment } from 'environments/environment';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { User } from 'app/core/user/model/user.model';
import { Store } from '@ngrx/store';
import {
  commandRequest,
  CommandRequestToServer,
} from 'app/core/command/model/command';
import { sendCommand } from 'app/core/command/store/command.action';

export interface DialogData {
  clientSecret: string;
  user: User;
  commands: commandRequest[];
}

export enum PaymentStatus {
  Form,
  Loading,
  Success,
  Error,
}

@Component({
  selector: 'app-dialog-stripe-test',
  styles: `
  @import 'button';
  @import 'loader';
  `,

  template: `
    <div style="padding: 1rem; margin: auto">
      @switch (paymentState()) { @case (PaymentStatusEnum.Form) {
      <form [formGroup]="paymentElementForm">
        <mat-form-field appearance="fill">
          <input matInput placeholder="name" formControlName="name" />
        </mat-form-field>
        <mat-form-field appearance="fill">
          <input
            matInput
            placeholder="Email"
            type="email"
            formControlName="email"
          />
        </mat-form-field>
        <mat-form-field appearance="fill">
          <input matInput placeholder="Address" formControlName="address" />
        </mat-form-field>
        <mat-form-field appearance="fill">
          <input matInput placeholder="ZIP Code" formControlName="zipcode" />
        </mat-form-field>
        <mat-form-field appearance="fill">
          <input matInput placeholder="city" formControlName="city" />
        </mat-form-field>
        @if (elementsOptions.clientSecret) {
        <ngx-stripe-elements
          [stripe]="stripe"
          [elementsOptions]="elementsOptions"
        >
          <ngx-stripe-payment [options]="paymentElementOptions" />
        </ngx-stripe-elements>
        }
      </form>
      <mat-dialog-actions>
        <button mat-dialog-close mat-button>Annuler</button>
        <button
          color="primary"
          mat-raised-button
          mat-button
          (click)="pay()"
          cdkFocusInitial
        >
          Payer
        </button>
      </mat-dialog-actions>
      } @case (PaymentStatusEnum.Loading) { } @case (PaymentStatusEnum.Success)
      {
      <h2>Payement réussi</h2>
      <div class="success-checkmark">
        <div class="check-icon">
          <span class="icon-line line-tip"></span>
          <span class="icon-line line-long"></span>
          <div class="icon-circle"></div>
          <div class="icon-fix"></div>
        </div>
      </div>
      <mat-dialog-actions>
        <button
          routerLink="[/menu]"
          color="primary"
          mat-dialog-close
          mat-button
          cdkFocusInitial
        >
          Retour aux menus
        </button>
      </mat-dialog-actions>

      } @case (PaymentStatusEnum.Error) {
      <h2>Une erreur est survenue</h2>
      <mat-dialog-actions>
        <button
          (click)="paymentState.set(PaymentStatusEnum.Form)"
          color="primary"
          mat-raised-button
          mat-button
          cdkFocusInitial
        >
          Ressayer
        </button>
      </mat-dialog-actions>
      } }
    </div>
  `,
  standalone: true,
  imports: [
    MatDialogActions,
    MatDialogClose,
    MatButton,
    ReactiveFormsModule,
    MatInputModule,
    StripeElementsDirective,
    StripePaymentElementComponent,
    RouterModule,
  ],
})
export class CheckoutFormComponent {
  constructor(
    public dialogRef: MatDialogRef<CheckoutFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}



  @ViewChild(StripePaymentElementComponent)
  paymentElement!: StripePaymentElementComponent;
  store = inject(Store);

  PaymentStatusEnum = PaymentStatus;

  private readonly fb = inject(UntypedFormBuilder);
  private readonly payementGateway = inject(PaymentGatewayService);

  paymentElementForm = this.fb.group({
    name: [
      this.data.user.lastName + ' ' + this.data.user.firstName,
      [Validators.required],
    ],
    email: [this.data.user.email, [Validators.required]],
    address: [this.data.user.addressStreet, [Validators.required]],
    zipcode: [this.data.user.addressZipCode, [Validators.required]],
    city: [this.data.user.addressCity, [Validators.required]],
    amount: [2500, [Validators.required, Validators.pattern(/d+/)]],
  });

  elementsOptions: StripeElementsOptions = {
    clientSecret: this.data.clientSecret,
    locale: 'fr',
    appearance: {
      theme: 'flat',
    },
  };

  paymentElementOptions: StripePaymentElementOptions = {
    layout: {
      type: 'tabs',
      defaultCollapsed: false,
      radios: false,
      spacedAccordionItems: false,
    },
  };

  // Replace with your own public key
  stripe = injectStripe(environment.stipe_public_key);
  paying = signal(false);
  paymentState = signal<PaymentStatus>(PaymentStatus.Form);

  pay() {
    if (this.paying()) {
      console.info('Payment is already in progress.');
      return;
    }

    this.paying.set(true);
    console.info('Payment process started.');

    const { name, email, address, zipcode, city } =
      this.paymentElementForm.getRawValue();
  

    this.stripe
      .confirmPayment({
        elements: this.paymentElement.elements,
        confirmParams: {
          payment_method_data: {
            billing_details: {
              name: name as string,
              email: email as string,
              address: {
                line1: address as string,
                postal_code: zipcode as string,
                city: city as string,
              },
            },
          },
        },
        redirect: 'if_required',
      })
      .subscribe(
        (result) => {
          this.paying.set(false);
          this.paymentState.set(this.PaymentStatusEnum.Loading);
     

          if (result.error) {
            // Show error to your customer (e.g., insufficient funds)
            this.paymentState.set(this.PaymentStatusEnum.Error);
               } else {
            // The payment has been processed!
            if (result.paymentIntent.status === 'succeeded') {
              // Show a success message to your customer
              this.paymentState.set(this.PaymentStatusEnum.Success);
       
              this.handlePaymentResponse(result.paymentIntent);
            } else {
              this.paymentState.set(this.PaymentStatusEnum.Error);

            }
          }
        },
        (error) => {
 
          this.paymentState.set(this.PaymentStatusEnum.Error);
          this.paying.set(false);
          alert({ success: false, error: error.message });
        }
      );
  }

  handlePaymentResponse(paymentIntent: any) {
    const commandRequest: CommandRequestToServer = {
      StripeInvoiceID: paymentIntent.id,
      StripePaymentMethod: paymentIntent.payment_method,
      StripeClientSecret: paymentIntent.client_secret,
      TotalPrice: Math.ceil(paymentIntent.amount/100),
      commands: this.data.commands,
    };


    this.store.dispatch(sendCommand({ commandRequest: commandRequest }));
  }
}
