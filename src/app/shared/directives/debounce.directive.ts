/* eslint-disable @typescript-eslint/no-explicit-any */
import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appDebounce]'
})
export class DebounceDirective {
  private timeoutId: any;

  constructor(private element: ElementRef) {}

  @HostListener('click')
  onClick() {
    this.element.nativeElement.disabled = true;
    this.timeoutId = setTimeout(() => {
      this.element.nativeElement.disabled = false;
    }, 2000);
    clearTimeout(this.timeoutId);
  }


}