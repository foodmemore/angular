import { ElementRef } from '@angular/core';
import { DebounceDirective } from './debounce.directive';

describe('DebounceDirective', () => {
  it('should create an instance', () => {
    const elementRef = new ElementRef(null); // Create a mock ElementRef instance
    const directive = new DebounceDirective(elementRef);
    expect(directive).toBeTruthy();
  });
});