import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faBars, faCircleUser } from '@fortawesome/free-solid-svg-icons';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ModalAuthComponent } from '../modals/modal-auth/modal-auth.component';
import { Store } from '@ngrx/store';
import * as UserActions from './../../core/user/store/user.action';
import { selectCurrentUseRoleState } from 'app/core/user/store/user.selector';
import { AsyncPipe } from '@angular/common';
import { UserCurrentUseRole } from 'app/core/user/store/user.state';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-nav-option',
  standalone: true,
  imports: [
    FontAwesomeModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    MatIconModule,
    RouterModule,
    AsyncPipe,
  ],
  template: `
    <aside [matMenuTriggerFor]="menu">
      <fa-icon class="bars" [icon]="faBars"></fa-icon>
      <fa-icon [icon]="faCircleUser"></fa-icon>
    </aside>
    <mat-menu class="menu" #menu="matMenu" xPosition="before">
      <ul>
        @if(userState$ | async; as userState) { @switch (userState) { @case
        (UserCurrentUseRole.NOT_LOGGED) {
        <li>
          <button [routerLink]="['/connexion']" mat-button>
            Connexion / Inscription
          </button>
        </li>
        } @case (UserCurrentUseRole.COOKER) {

        <li [routerLink]="['/preparation']" [routerLinkActive]="['active', '']">
          <button mat-button>Mes préparation</button>
        </li>
        <li [routerLink]="['/commande']" [routerLinkActive]="['active', '']">
          <button mat-button>Commande</button>
        </li>

        <li [routerLink]="['/profile']" [routerLinkActive]="['active', '']">
          <button mat-button>Mon profil</button>
        </li>

        <li>
          <button (click)="logout()" mat-button>Se déconnecter</button>
        </li>
        } @case (UserCurrentUseRole.ADMIN) {

        <li [routerLink]="['/admin']" [routerLinkActive]="['active', '']">
          <button mat-button>Gestion administrative</button>
        </li>


        <li>
          <button (click)="logout()" mat-button>Se déconnecter</button>
        </li>
        } @case (UserCurrentUseRole.USER) {
        <li [routerLink]="['/menu']" [routerLinkActive]="['active', '']">
          <button mat-button>Menu</button>
        </li>
        <li [routerLink]="['/panier']" [routerLinkActive]="['active', '']">
          <button mat-button>Panier</button>
        </li>
        <li
          [routerLink]="['/commande/utilisateur']"
          [routerLinkActive]="['active', '']"
        >
          <button mat-button>Commande</button>
        </li>
        <li [routerLink]="['/profile']" [routerLinkActive]="['active', '']">
          <button mat-button>Mon profil</button>
        </li>
        } } }
      </ul>
    </mat-menu>
  `,
  styles: `
  @import "color";
  aside {
    display: flex;
    justify-content: space-between;
    align-items: center;
    border: $border;
    border-radius: 100rem;
    padding: 10px 15px;
    width: 86px;
    cursor: pointer;
    transition: 0.2s ease-in-out;


    fa-icon {
      font-size: 20px;
      &.bars {
        font-size: 15px;
      }
    }
  }


  ul {

    width: 280px !important;
    li {
      cursor: pointer;
      border-radius: 100rem;
      transition: 0.2s ease-in-out;
      button{
        padding-left: 20px !important;
        padding-right: 60px !important;
        margin: 10px 0 !important;
        width: 100%;
        border-radius: 0 !important;
        display: flex;
        justify-content: start;
      }
      &:hover {
        background-color: $secondBackgroundLight;
      }

      &.active {
        border-radius: 0 !important;
        background-color: $primaryColorLightSelected;
      }
    }
  }
  .mat-elevation-z8{
    border-radius: 10rem !important;
  }

  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavOptionComponent {
  readonly IS_LOGGING = true;
  readonly IS_REGISTER = false;
  readonly dialog = inject(MatDialog);
  faBars = faBars;
  faCircleUser = faCircleUser;
  private readonly store = inject(Store);
  //  private readonly router = inject(Router);
  userState$ = this.store.select(selectCurrentUseRoleState);
  UserCurrentUseRole = UserCurrentUseRole;

  openDialog(type: boolean): void {
    this.dialog.open(ModalAuthComponent, {
      width: '60svw',
      height: '90svh',
      maxWidth: '600px',
      data: {
        type,
      },
    });
  }

  logout() {
    this.store.dispatch(UserActions.logout());
  }
}
