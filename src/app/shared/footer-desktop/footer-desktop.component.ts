import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-footer-desktop',
  standalone: true,
  imports: [RouterModule],
  template: `
    <ul class="wrap">
      <li>© FoodMeMore</li>
      <li><a [routerLink]="['/condition/confidentialite']" routerLinkActive="router-link-active"  >Confidentialité</a></li>
      <li><a [routerLink]="['/condition/cgu']" routerLinkActive="router-link-active" >Condition général d'utilisation</a></li>
      <li>Projet Scolaire</li>
    </ul>
  `,
  styles: `
@import "wrap";  
@import "color";
ul {
  height: 42px;
  display: flex;
  align-items: center;
  li {
    padding: 0 0.5rem;
    a {
      &:hover {
        text-decoration: underline;
      }
    }
  }
}  
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterDesktopComponent {

}
