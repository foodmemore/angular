import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { NavOptionComponent } from '../nav-option/nav-option.component';
import { LiCommendComponent } from "../navbar/nav/child/li-commend/li-commend.component";

@Component({
    selector: 'app-navbar-desktop',
    standalone: true,
    template: `
    <nav class="wrap">
      <span routerLink="menu" class="logo">
        <fa-icon [icon]="faUtensils"></fa-icon>
        <p>foodmemore</p>
      </span>
      <span class="center"> </span>
      <div class="option">
        <app-nav-option />
      </div>
    </nav>
  `,
    styles: `
@import "color";  
@import "wrap";
nav {

  display: flex;
  justify-content: space-between;
  span {
    display: flex;
  }
  .logo {
    display: flex;
    align-items: center;
    font-size: 1.6rem;
    padding: 0 1rem;
    cursor: pointer;
    p {
      margin-left: 1rem;
    }
    .option {
      display: flex;
      flex-direction: row;
    }
  }
}  
  `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [FontAwesomeModule, RouterOutlet, RouterModule, NavOptionComponent, LiCommendComponent]
})
export class NavbarDesktopComponent {
  faUtensils = faUtensils;
}
