import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MAT_DIALOG_DATA,
  MatDialogTitle,
} from '@angular/material/dialog';
import { LoginComponent } from '../../../core/auth/form/login/login.component';
import { LoginRegisterComponent } from '../../../routes/auth/login-register/login-register.component';
import { MatIcon } from '@angular/material/icon';

export interface DialogData {
  type: boolean;
}

@Component({
  selector: 'app-modal-auth',
  standalone: true,
  template: `
    <h2 mat-dialog-title>
      Connexion / Inscription
      <button
        mat-icon-button
        mat-dialog-close
        style="position: absolute; right: 8px; top: 8px;"
      >
        <mat-icon>close</mat-icon>
      </button>
    </h2>

    <app-login-register [isLogging]="data.type"></app-login-register>
  `,
  styles: `
    
    `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatButtonModule,
    MatDialogActions,
    MatDialogClose,
    MatDialogTitle,
    MatDialogContent,
    LoginComponent,
    LoginRegisterComponent,
    MatIcon,
  ],
})
export class ModalAuthComponent {
  // constructor(public readonly dialogRef: MatDialogRef<ModalAuthComponent>, ) {  }
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
}
