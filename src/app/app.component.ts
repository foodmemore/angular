import { Component, OnInit, inject } from '@angular/core';
import { AsyncPipe, CommonModule, JsonPipe } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { NavbarDesktopComponent } from './shared/navbar-desktop/navbar-desktop.component';
import { FooterDesktopComponent } from './shared/footer-desktop/footer-desktop.component';
import { RefreshTokensComponent } from './shared/utils/refresh-tokens/refresh-tokens.component';
import { Store } from '@ngrx/store';
import { StoreAllergenSelectorComponent } from './shared/components/store/store-allergen/store-allergen.component';
import { selectTriggerGetMenus } from './core/menu/store/menu.selector';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [
    AsyncPipe,
    JsonPipe,
    CommonModule,
    RouterOutlet,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    NavbarComponent,
    NavbarDesktopComponent,
    FooterDesktopComponent,
    RefreshTokensComponent,
    StoreAllergenSelectorComponent,
  ],
})
export class AppComponent implements OnInit {
  title = 'angular';
  readonly store = inject(Store);

  triggerGetMenus$ = this.store.select(selectTriggerGetMenus);
  ngOnInit(): void {
    if (environment.production) {
      if (window.location.protocol === 'http:') {
        window.location.href = window.location.href.replace('http:', 'https:');
      }
    }
  }
}
