export const MY_DATES_FORMATS = {
    parse: {
        dateInput: ['DD/MM/YYYY','MM/DD/YYYY', 'yyyy-MM-dd'],
    },
    display: {
        dateInput: 'dd/mm/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
    }
};
