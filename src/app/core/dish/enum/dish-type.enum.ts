export enum DishType {
    MainDish = 'main_dish',
    Entree = 'entree',
    Dessert = 'dessert',
    NotActive = 'not_active'
}