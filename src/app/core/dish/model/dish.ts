import { Allergen } from 'app/core/allergen/model/Allergen';
import { DishType } from '../../dish/enum/dish-type.enum';

export type Dish = {
  ID: number;
  Type?: DishType;
  ImgSrc: string;
  Description: string;
  Allergens: Allergen[]
  Price: number;
  CookerID: number;
  Title: string;
  CreatedAt: Date | string;
  UpdatedAt: Date | string;
};
