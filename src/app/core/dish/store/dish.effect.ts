import { Injectable, inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { DishGatewayService } from '../gateway/dish-gateway.service';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';
import * as DishActions from './dish.action';
import { catchError, map, of, switchMap } from 'rxjs';
import { Dish } from '../model/dish';

@Injectable()
export class DishEffect {
  private readonly store = inject(Store);
  private readonly actions$ = inject(Actions);
  private readonly dishGatewayService = inject(DishGatewayService);
  private readonly snackBar = inject(SnackbarService);
  constructor() {}

  getDishes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DishActions.GET_DISHES_LOADING),
      switchMap(() =>
        this.dishGatewayService.getAll().pipe(
          map((dishList) => DishActions.getDishesSuccess({ dishList: dishList })),
          catchError(() =>
            of(DishActions.getDishesFail({ error: 'Erreur chargement' }))
          )
        )
      )
    );
  });

  getDishesSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DishActions.getDishesSuccess),
        map(() => {
          this.snackBar.showSuccess('Dishes charge');
        })
      );
    },
    { dispatch: false }
  );

  getDishesFail$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DishActions.getDishesFail),
        map(() => {
          this.snackBar.showError('Erreur chargement');
        })
      );
    },
    { dispatch: false }
  );

  addDishes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DishActions.ADD_DISHES_LOADING),
      switchMap((action) =>
        this.dishGatewayService.postFormData(action.dishFormData).pipe(
          map((dish: Dish) => DishActions.addDishesSuccess({dish})),
          catchError(() =>
            of(DishActions.addDishesFail({ error: 'Erreur ajout' }))
          )
        )
      )
    );
  });

  addDishesSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DishActions.addDishesSuccess),
        map(() => {
          this.snackBar.showSuccess('Plat ajouté');
        })
      );
    },
    { dispatch: false }
  );

  addDishesFail$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DishActions.addDishesFail),
        map(() => {
          this.snackBar.showError('Erreur ajout');
        })
      );
    },
    { dispatch: false }
  );

}
