import { createFeatureSelector, createSelector } from "@ngrx/store";
import { DishState } from "./dish.state";


const selectDishState = createFeatureSelector<DishState>('dish');

export const selectDishListState = createSelector(
    selectDishState,
    (state: DishState) => state.dishList
);

export const selectDishCurrentState = createSelector(
    selectDishState,
    (state: DishState) => state.currentState
)