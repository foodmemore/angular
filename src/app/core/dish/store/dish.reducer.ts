/* eslint-disable @typescript-eslint/no-explicit-any */
import { Action, createReducer, on } from '@ngrx/store';
import { DishState, dishState } from './dish.state';
import { DishType } from '../enum/dish-type.enum';
import * as DishActions from './dish.action';

const _DishReducer = createReducer<DishState>(
  dishState,

  on(DishActions.addDishes, (state: DishState): DishState => {
    return {
      ...state,
      isLoading: true,
      isError: false,
      isUpdating: false,
    };
  }),

  on(DishActions.addDishesSuccess, (state: DishState, { dish }): DishState => {
    const newState: DishState = {
      ...state,
      dishList: [...state.dishList, dish],
      isLoading: false,
      isError: false,
      isUpdating: false,
      currentState: DishType.NotActive
    };
    return newState;
  }),

  on(DishActions.addDishesFail, (state): DishState => {
    return {
      ...state,
      isLoading: false,
      isError: true,
      isUpdating: false,
    };
  }),

  on(
    DishActions.getDishesSuccess,
    (state: DishState,  dishList ): DishState => {
      return {
        ...state,
        dishList : dishList.dishList,
      };
    }
  ),

  on(DishActions.setCurrentState, (state: DishState, { currentState }): DishState => {
    return {
      ...state,
      currentState
    };
  })
);

export function DishReducer(state: any, action: Action) {
  return _DishReducer(state, action);
}
