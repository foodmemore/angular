import { createAction, props } from "@ngrx/store";
import { Dish } from "../model/dish";
import { DishType } from "../enum/dish-type.enum";


export const GET_DISHES_LOADING = '[dishes] get dishes loading';
export const GET_DISHES_SUCCESS = '[dishes] get dishes success';
export const GET_DISHES_FAIL = '[dishes] get dishes fail';

export const ADD_DISHES_LOADING = '[dishes] add dishes loading';
export const ADD_DISHES_SUCCESS = '[dishes] add dishes success';
export const ADD_DISHES_FAIL = '[dishes] add dishes fail';

export const SET_CURRENT_STATE = '[dishes] set dishes state';
export const setCurrentState = createAction(SET_CURRENT_STATE, props<{ currentState: DishType }>());

export const getDishes = createAction(GET_DISHES_LOADING);
export const getDishesSuccess = createAction(GET_DISHES_SUCCESS, props<{ dishList: Dish[] }>());
export const getDishesFail = createAction(GET_DISHES_FAIL, props<{ error: string }>());

export const addDishes = createAction(ADD_DISHES_LOADING, props<{ dishFormData: FormData }>());
export const addDishesSuccess = createAction(ADD_DISHES_SUCCESS, props<{ dish: Dish }>());
export const addDishesFail = createAction(ADD_DISHES_FAIL, props<{ error: string }>());