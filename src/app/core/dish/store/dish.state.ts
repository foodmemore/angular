import { DishType } from '../enum/dish-type.enum';
import { Dish } from '../model/dish';

export interface DishState {
  dishToPost: Dish;
  dishList: Dish[];
  isLoading: boolean;
  isError: boolean;
  isUpdating: boolean;
  currentState: DishType
}


export const dishState: DishState = {
  dishToPost: {
    ID: -1,
    Type: undefined,
    ImgSrc: '',
    Description: '',
    Price: 0,
    CookerID: -1,
    Title: '',
    Allergens: [],
    CreatedAt: new Date(),
    UpdatedAt: new Date(),
  },
  dishList: [],
  isLoading: false,
  isError: false,
  isUpdating: false,
  currentState: DishType.NotActive
}