import { Injectable } from '@angular/core';
import { Dish } from '../model/dish';
import { GatewayHelper } from '../../helpers/gateway.helper';

@Injectable({
  providedIn: 'root'
})
export class DishGatewayService extends GatewayHelper<Dish> {
  constructor() {
    super('/dish');
  }
}
