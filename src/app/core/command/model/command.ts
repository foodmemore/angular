import { Menu } from "app/core/menu/model/menu";

export interface Command {
  ID: number;
  CreatedAt: Date;
  UpdatedAt: Date;
  CustomerID: number;
  TotalPrice: number;
  StripeInvoiceID: string;
  StripePaymentMethod: string;
  StripeClientSecret: string;

}

export interface MenuCommand {
  price: number;
  entreeID: number;
  mainDishID: number;
  dessertID: number;
}

export interface commandRequest {
  date: Date;
  hour: number;
  minute: number;
  price: number;
  menuID: number;
  menuCommands: MenuCommand[];
}

export interface CommandRequestToServer {
  commands: commandRequest[];
  TotalPrice: number;
  StripeInvoiceID: string;
  StripePaymentMethod: string;
  StripeClientSecret: string;
}

export interface MenuCommandResponse {
  ID: number;
  CreatedAt: Date;
  UpdatedAt: Date;
  CommandID: number;
  DayToDeliver: Date;
  MinuteToDeliver: number;
  HourToDeliver: number;
  EntreeID: number;
  MainDishID: number;
  DessertID: number;
  
  MenuID: number;
  Menu: Menu;
  Command: Command;
  Price: number;
  countNumber?: number;
  totalMenuPrice?: number;
  totalEntreNumber?: [];
  totalMainDishNumber?: [];
  totalDessertNumber?: [];
}

export interface MenuCommandResponseWellTyped {
  ID: number;
  CreatedAt: Date;
  UpdatedAt: Date;
  CommandID: number;
  DayToDeliver: Date;
  MinuteToDeliver: number;
  HourToDeliver: number;
  EntreeID: number;
  MainDishID: number;
  DessertID: number;
  
  MenuID: number;
  Menu: Menu;
  Command: Command;
  Price: number;
  countNumber: number;
  totalMenuPrice: number;
  totalEntreNumber: [];
  totalMainDishNumber: [];
  totalDessertNumber: [];
}

export interface MenuCommandUserResponse {
  ID: number;
  CreatedAt: Date;
  UpdatedAt: Date;
  CommandID: number;
  DayToDeliver: Date;
  MinuteToDeliver: number;
  HourToDeliver: number;

  MenuID: number;
  Menu: MenuCommandResponse;
  Command: Command;
  Price: number;
}

export interface MenuCommandUserResponseWellTyped {
  ID: number;
  CreatedAt: Date;
  UpdatedAt: Date;
  CommandID: number;
  DayToDeliver: Date;
  MinuteToDeliver: number;
  HourToDeliver: number;

  MenuID: number;
  Menu: MenuCommandResponseWellTyped;
  Command: Command;
  Price: number;
}