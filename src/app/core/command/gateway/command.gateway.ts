/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { GatewayHelper } from 'app/core/helpers/gateway.helper';
import { Observable } from 'rxjs';
import { MenuCommandResponse, MenuCommandUserResponse } from '../model/command';

@Injectable({
  providedIn: 'root',
})
export class CommandGatewayService extends GatewayHelper<any> {
  constructor() {
    super('/command');
  }



  public getMenuCommandCooker(): Observable<MenuCommandResponse[]> {
    return this.http.get<[]>(this.apiUrl + 'menu/cooker', { headers: this.headers });
  }

  public getMenuCommandUser(): Observable<MenuCommandUserResponse[]> {
    return this.http.get<[]>(this.apiUrl + 'menu/user', { headers: this.headers });
  }

}
