import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CommandState } from './command.state';

const selectCommandState = createFeatureSelector<CommandState>('command');

export const selectCurrentCommandState = createSelector(
  selectCommandState,
  (state: CommandState) => state
);

export const selectCurrentCommand = createSelector(
  selectCurrentCommandState,
  (state: CommandState) => state.command
);

export const selectCommandNumber = createSelector(
  selectCurrentCommandState,
  (state: CommandState) => state.commands.length
);

export const selectCommands = createSelector(
  selectCurrentCommandState,
  (state: CommandState) => state.commands
);

export const selectCommandAnimation = createSelector(
  selectCurrentCommandState,
  (state: CommandState) => state
)