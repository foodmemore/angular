/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAction, props } from '@ngrx/store';
import {
  CommandRequestToServer,
  MenuCommand,
  commandRequest,
} from '../model/command';

export const CREATE_NEW_COMMAND = '[command] create new command';
export const CREATE_NEW_COMMAND_SUCCESS =
  '[command] create new command success';

export const createNewCommand = createAction(
  CREATE_NEW_COMMAND,
  props<{ command: commandRequest }>()
);
export const createNewCommandSuccess = createAction(
  CREATE_NEW_COMMAND_SUCCESS,
  props<{ command: commandRequest }>()
);

export const SET_COMMAND_MENU = '[command] set command menu';

export const setCommandMenu = createAction(
  SET_COMMAND_MENU,
  props<{ menuCommands: MenuCommand[] }>()
);

export const SET_DATE_COMMAND = '[command] set date command';
export const setDateCommand = createAction(
  SET_DATE_COMMAND,
  props<{ date: Date, hour: number, minute: number }>()
);

export const SET_MENU_ID_COMMAND = '[command] set menu ID command';
export const setMenuIDCommand = createAction(
  SET_MENU_ID_COMMAND,
  props<{ id: number }>()
);

export const SET_NUMBER_COMMAND = '[command] set number command';
export const setNumberCommand = createAction(
  SET_NUMBER_COMMAND,
  props<{ index: number; menuCommand: MenuCommand }>()
);

export const ADD_CURRENT_COMMAND = '[command] add command';
export const addCurrentCommand = createAction(ADD_CURRENT_COMMAND);

export const TOGGLE_ANIMATION = '[command] set action state';
export const toggleAnimation = createAction(
  TOGGLE_ANIMATION,
  props<{ changeActive: boolean }>()
);

export const SEND_COMMAND = '[command] send command';
export const sendCommand = createAction(SEND_COMMAND, props<{ commandRequest: CommandRequestToServer }>());

export const SEND_COMMAND_SUCCESS = '[command] send command success';
export const sendCommandSuccess = createAction(SEND_COMMAND_SUCCESS, props<{ message: string, response: any }>());
export const SEND_COMMAND_FAIL = '[command] send command fail';
export const sendCommandFail = createAction(SEND_COMMAND_FAIL, props<{ message: string }>());
