/* eslint-disable @typescript-eslint/no-explicit-any */

import { Injectable, inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { CommandGatewayService } from '../gateway/command.gateway';
import { CommandRequestToServer } from '../model/command';
import * as CommandActions from './command.action';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';
import { Router } from '@angular/router';
import { catchError, map, of, switchMap } from 'rxjs';

interface SendCommandAction {
  type: typeof CommandActions.SEND_COMMAND;
  commandRequest: CommandRequestToServer;
}

@Injectable()
export class CommandEffect {
  private readonly store: Store = inject(Store);
  private readonly actions$: Actions = inject(Actions);
  private readonly commandGatewayService: CommandGatewayService = inject(
    CommandGatewayService
  );
  private readonly snackBar: SnackbarService = inject(SnackbarService);
  private readonly router: Router = inject(Router);

  $sendCommand = createEffect(() => {
    return this.actions$.pipe(
      ofType(CommandActions.SEND_COMMAND),
      switchMap((action: SendCommandAction) =>
        this.commandGatewayService.post(action.commandRequest).pipe(
          map((response) =>
            CommandActions.sendCommandSuccess({
              message: 'Commande envoyée',
              response: response,
            })
          ),
          catchError(() => of(CommandActions.sendCommandFail({
            message: 'Une erreur est survenue',})))
        )
      )
    );
  });


  
}
