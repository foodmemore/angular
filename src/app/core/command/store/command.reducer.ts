/* eslint-disable @typescript-eslint/no-explicit-any */
import { Action, createReducer, on } from '@ngrx/store';
import { CommandState, commandState } from './command.state';
import * as CommandActions from './command.action';
import { MenuCommand } from '../model/command';

const _CommandReducer = createReducer<CommandState>(
  commandState,

  on(CommandActions.createNewCommand, (state, { command }): CommandState => {
    return {
      ...state,
      command: command,
      loading: false,
      error: false,
    };
  }),

  on(
    CommandActions.createNewCommandSuccess,
    (state, { command }): CommandState => {
      return {
        ...state,
        command: {
          hour: 10,
          minute: 0,
          date: new Date(),
          price: 0,
          menuID: 0,
          menuCommands: [],
        },
        commands: [...state.commands, command],
        loading: false,
        error: false,
      };
    }
  ),

  on(CommandActions.setCommandMenu, (state, { menuCommands }): CommandState => {
    return {
      ...state,
      command: {
        ...state.command,
        menuCommands: menuCommands,
      },
      loading: false,
      error: false,
    };
  }),

  on(CommandActions.setMenuIDCommand, (state, { id }): CommandState => {
    return {
      ...state,
      command: {
        ...state.command,
        menuID: id,
      },
      loading: false,
      error: false,
    };
  }),

  on(
    CommandActions.setNumberCommand,
    (state, { index, menuCommand }): CommandState => {
      const updatedMenuCommands = [...state.command.menuCommands]; 
      updatedMenuCommands[index] = menuCommand;

      return {
        ...state,
        command: {
          ...state.command,
          menuCommands: updatedMenuCommands,
          price: calculateTotalPrice(updatedMenuCommands),
        },
        loading: false,
        error: false,
      };
    }
  ),

   on(CommandActions.addCurrentCommand, (state): CommandState => {
     return {
       ...state,
       command: {
        date: new Date(),
        hour: 10,
        minute: 0,
        price: 0,
        menuID: 0,
        menuCommands: [],
       },
       commands: [...state.commands, state.command],
       loading: false,
       error: false,
       changeActive: true,
     };
   }),

   on(CommandActions.setDateCommand, (state, { date, hour, minute }): CommandState => {
     return {
       ...state,
       command: {
         ...state.command,
         date: date,
         hour : hour,
         minute: minute
       },
       loading: false,
       error: false,
     };
   }),

   on(CommandActions.toggleAnimation, (state, { changeActive }): CommandState => {
     return {
       ...state,
       changeActive: changeActive,
     };
   }),

   on(CommandActions.sendCommand, (state): CommandState => {
     return {
       ...state,
       commands: [],
       loading: true,
       error: false,
     };
   }),


   on(CommandActions.sendCommandSuccess, (state): CommandState => {
     return {
       ...state,
       commands: [],
       loading: false,
       error: false,
     };
   }),
);

export function CommandReducer(state: any, action: Action) {
  return _CommandReducer(state, action);
}
const calculateTotalPrice = (menuCommands: MenuCommand[]): number => {
  return menuCommands.reduce((acc, item) => acc + item.price, 0);
};
