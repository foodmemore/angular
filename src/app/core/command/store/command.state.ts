import { commandRequest } from "../model/command";

export interface CommandState {
    commands: commandRequest[];
    command: commandRequest;
    loading: boolean;
    error: boolean;
    changeActive: boolean;
}

export const commandState: CommandState = {
    commands: [],
    command: {
        date: new Date(),
        hour: 10,
        minute: 0,
        price: 0,
        menuID: 0,
        menuCommands: [],
    },
    loading: false,
    error: false,
    changeActive: false,
}