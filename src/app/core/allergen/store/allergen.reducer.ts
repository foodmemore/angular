/* eslint-disable @typescript-eslint/no-explicit-any */
import { Action, createReducer, on } from '@ngrx/store';
import { allergenState, AllergenState } from './allergen.state';
import * as AllergenActions from './allergen.action';

const _AllergenReducer = createReducer<AllergenState>(
  allergenState,

  on(
    AllergenActions.getAllergenSuccess,
    (state: AllergenState, { allergenList }): AllergenState => {
      return {
        ...state,
        isLoading: false,
        isError: false,
        allergenList,
      };
    }
  )
);

export function AllergenReducer(state: any, action: Action) {
  return _AllergenReducer(state, action);
}
