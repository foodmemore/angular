import { createAction, props } from "@ngrx/store";
import { Allergen } from "../model/Allergen";

export const GET_ALLERGEN_LOADING = '[allergen] get allergen loading';
export const GET_ALLERGEN_SUCCESS = '[allergen] get allergen success';
export const GET_ALLERGEN_FAIL = '[allergen] get allergen fail';

export const getAllergen = createAction(GET_ALLERGEN_LOADING);
export const getAllergenSuccess = createAction(GET_ALLERGEN_SUCCESS, props<{ allergenList: Allergen[] }>());
export const getAllergenFail = createAction(GET_ALLERGEN_FAIL, props<{ error: string }>())