import { Allergen } from "../model/Allergen";


export interface AllergenState {
    allergenList: Allergen[];
    isLoading: boolean;
    isError: boolean;
}

export const allergenState: AllergenState = {
    allergenList: [],
    isLoading: false,
    isError: false
}