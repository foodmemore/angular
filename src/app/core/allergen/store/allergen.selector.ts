import { createFeatureSelector, createSelector } from '@ngrx/store';

import { AllergenState } from './allergen.state';

const selectAllergenState = createFeatureSelector<AllergenState>('allergen');

export const selectAllergenListState = createSelector(
  selectAllergenState,
  (state: AllergenState) => state.allergenList
);
