import { inject, Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { AllergenGatewayService } from "../gateway/allergen.gateway.service";
import * as AllergenActions from './allergen.action';
import { switchMap, map, catchError, of } from "rxjs";

@Injectable()
export class AllergenEffect {
    private readonly actions$ = inject(Actions);
    private readonly allergenGatewayService = inject(AllergenGatewayService);
    constructor() {}

    getAllergen$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(AllergenActions.GET_ALLERGEN_LOADING),
            switchMap(() =>
                this.allergenGatewayService.getAll().pipe(
                    map((allergenList) => AllergenActions.getAllergenSuccess({ allergenList: allergenList })),
                    catchError(() =>
                        of(AllergenActions.getAllergenFail({ error: 'Erreur chargement' }))
                    )
                )
            )
        );
    });
    
}