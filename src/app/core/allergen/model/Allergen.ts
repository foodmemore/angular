export interface Allergen {
    ID: number;
    Libelle: string;
    Description: string;
}