import { Injectable } from "@angular/core";
import { GatewayHelper } from "app/core/helpers/gateway.helper";
import { Allergen } from "../model/Allergen";

@Injectable({
    providedIn: 'root',
  })
  export class AllergenGatewayService extends GatewayHelper<Allergen> {
    constructor() {
      super('/allergen');
    }
}