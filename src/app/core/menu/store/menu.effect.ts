import { inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';
import { MenuGatewayService } from '../gateway/menu.service';
import {
  catchError,
  debounceTime,
  filter,
  map,
  of,
  switchMap,
  withLatestFrom,
} from 'rxjs';
import * as MenuActions from './menu.action';
import { selectMenuPagination, selectTriggerGetMenus } from './menu.selector';
import { Menu } from '../model/menu';

export class MenuEffect {
  private readonly store = inject(Store);
  private readonly actions$ = inject(Actions);
  private readonly menuGatewayService = inject(MenuGatewayService);
  private readonly snackBar = inject(SnackbarService);

  getMenus$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.GET_MENU_LOADING),
      debounceTime(300),
      // eslint-disable-next-line @ngrx/prefer-concat-latest-from
      withLatestFrom(this.store.select(selectMenuPagination)),
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      switchMap(([action, pagination]) =>
        this.menuGatewayService.getAllMenus(pagination).pipe(
          map(
            (menuList) => {
              if (menuList.length > 0) {
             
                // eslint-disable-next-line @ngrx/no-dispatch-in-effects
                this.store.dispatch(MenuActions.incrementPagination({ menuList: menuList }));
            
                return MenuActions.getMenus();
              } else {
                return MenuActions.getMenusSuccess();
              }
            },

            catchError(() =>
              of(MenuActions.getMenusFail({ error: 'Erreur chargement' }))
            )
          )
        )
      )
    );
  });

  toggleMenuAvailability$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.toggleMenuAvailability),
      debounceTime(1), 
     switchMap((action) => {
       return this.menuGatewayService
         .toggleMenuAvailability(action.menuID)
         .pipe(
           map((menu: Menu) => {
             return MenuActions.toggleMenuAvailabilitySuccess({menu});
           }),
         );
     })
    );
  });

  toggleMenuAvailabilitySuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.toggleMenuAvailabilitySuccess),
    );
    
  },
  { dispatch: false }
);

  triggerGetMenus$ = createEffect(() => {
    return this.store.select(selectTriggerGetMenus).pipe(
      filter(state => state), 
      map(() => MenuActions.getMenus()), // Déclenche l'action `getMenus`
      // Réinitialise l'indicateur `triggerGetMenus`
      map(() => ({ type: '[Menu] Reset Trigger Get Menus' }))
    );
  });

  doIncrementPagination$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(MenuActions.incrementPagination),
        map(() => {
          return MenuActions.getMenus();
        })
      );
    },
    { dispatch: false }
  );

  getMenusSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(MenuActions.getMenusSuccess),
        map(() => {
          this.snackBar.showSuccess('Menus charge');
        })
      );
    },
    { dispatch: false }
  );

  getMenusFail$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(MenuActions.getMenusFail),
        map(() => {
          this.snackBar.showError('Erreur chargement');
        })
      );
    },
    { dispatch: false }
  );

  getMenusCooker$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.GET_MENU_COOKER_LOADING),
      switchMap(() =>
        this.menuGatewayService.getAllMenusCooker().pipe(
          map((menuList) =>
            MenuActions.getMenusCookerSuccess({ menuList: menuList })
          ),
          catchError(() =>
            of(MenuActions.getMenusFail({ error: 'Erreur chargement' }))
          )
        )
      )
    );
  });

  getMenusCookerSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(MenuActions.getMenusSuccess),
        map(() => {
          this.snackBar.showSuccess('Menus charge');
        })
      );
    },
    { dispatch: false }
  );

  addMenuCooker$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.ADD_MENU_LOADING),
      switchMap((action) =>
        this.menuGatewayService.postMenuCooker(action.newMenu).pipe(
          map((menu) => MenuActions.addMenuCookerSuccess({ menu: menu })),
          catchError(() =>
            of(MenuActions.addMenuCookerFail({ error: 'Erreur ajout' }))
          )
        )
      )
    );
  });

  addMenuCookerSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(MenuActions.addMenuCookerSuccess),
        map(() => {
          this.snackBar.showSuccess('Menu ajoute');
        })
      );
    },
    { dispatch: false }
  );

  addMenuCookerFail$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(MenuActions.addMenuCookerFail),
        map(() => {
          this.snackBar.showError('Erreur ajout');
        })
      );
    },
    { dispatch: false }
  );
}
