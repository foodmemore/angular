import { createFeatureSelector, createSelector } from "@ngrx/store";
import { MenuState } from "./menu.state";


const selectMenuState = createFeatureSelector<MenuState>('menu');

export const selectMenuListState = createSelector(
    selectMenuState,
    (state: MenuState) => state.menuList
);

export const selectMenuCookerListState = createSelector(
    selectMenuState,
    (state: MenuState) => state.menuCookerList
)
export const selectMenuPagination = createSelector(
    selectMenuState,
    (state: MenuState) => state.menuPagination
)

export const selectMenuStateGlobal = createSelector(
    selectMenuState,
    (state: MenuState) => state
)

export const selectTriggerGetMenus = createSelector(
    selectMenuStateGlobal,
    (state) => state.triggerGetMenus
  );