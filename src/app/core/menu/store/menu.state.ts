import { Menu, MenuCookerToPost, MenuRequest } from "../model/menu";


export interface MenuState {
    isLoading: boolean;
    isError: boolean;
    menuList: Menu[];
    menuCookerList: Menu[];
    menuToPost: MenuRequest;
    menuCookerToPost: MenuCookerToPost;
    menuPagination: number;
    triggerGetMenus: boolean;
}


export const menuState: MenuState = {
    menuPagination: 0,
    isLoading: false,
    isError: false,
    triggerGetMenus: true,
    menuList: [],
    menuCookerList: [],
    menuToPost: {
        ID: -1,
        Open: false,
        EntreeID: -1,
        MainDishID: -1,
        DessertID: -1,
        Description: '',
        PartialPrice: 0,
        TotalPrice: 0,
    },
    menuCookerToPost: {
        entreeId: -1,
        mainDishId: -1,
        dessertId: -1,
        description: '',
        partielPrice: 0,
        totalPrice: 0
    }
}