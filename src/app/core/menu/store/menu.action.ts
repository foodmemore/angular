import { createAction, props } from "@ngrx/store";
import { Menu, MenuCookerToPost } from "../model/menu";

export const GET_MENU_LOADING = '[menu] get menu loading';
export const GET_MENU_COOKER_LOADING = '[menu] get menu cooker loading';
export const GET_MENU_SUCCESS = '[menu] get menu success';
export const GET_MENU_FAIL = '[menu] get menu fail';

export const ADD_MENU_LOADING = '[menu] add menu loading';
export const ADD_MENU_SUCCESS = '[menu] add menu success';
export const ADD_MENU_FAIL = '[menu] add menu fail';


export const getMenus = createAction(GET_MENU_LOADING);
export const getMenusSuccess = createAction(GET_MENU_SUCCESS)
export const getMenusFail = createAction(GET_MENU_FAIL, props<{ error: string }>());
export const incrementPagination = createAction('[Menu] Increment Pagination', props<{menuList: Menu[]}>());


export const getMenusCooker = createAction(GET_MENU_COOKER_LOADING);
export const getMenusCookerSuccess = createAction(GET_MENU_SUCCESS, props<{ menuList: Menu[] }>());
export const getMenusCookerFail = createAction(GET_MENU_FAIL, props<{ error: string }>());

// export const addMenus = createAction(ADD_MENU_LOADING, props<{ newMenu: Menu }>());
// export const addMenusSuccess = createAction(ADD_MENU_SUCCESS, props<{ menu: Menu }>());
// export const addMenusFail = createAction(ADD_MENU_FAIL, props<{ error: string }>());

export const addMenuCooker = createAction(ADD_MENU_LOADING, props<{ newMenu: MenuCookerToPost }>());
export const addMenuCookerSuccess = createAction(ADD_MENU_SUCCESS, props<{ menu: Menu }>());
export const addMenuCookerFail = createAction(ADD_MENU_FAIL, props<{ error: string }>());

export const toggleMenuAvailability = createAction('[Menu] toggle menu availability', props<{ menuID: number }>());
export const toggleMenuAvailabilitySuccess = createAction('[Menu] toggle menu availability', props<{ menu: Menu }>());