/* eslint-disable @typescript-eslint/no-explicit-any */
import { MenuState, menuState } from './menu.state';
import * as MenuActions from './menu.action';
import { Action, createReducer, on } from '@ngrx/store';

const _MenuReducer = createReducer<MenuState>(
  menuState,

  on(MenuActions.getMenus, (state: MenuState): MenuState => {
    return {
      ...state,
      triggerGetMenus: false,
    };
  }),

  on(
    MenuActions.getMenusCookerSuccess,
    (state: MenuState, { menuList }): MenuState => {
      if (!menuList) {
        return state;
      }
      return {
        ...state,
        menuCookerList: menuList,
        isLoading: false,
        isError: false,
        triggerGetMenus: false,
      };
    }
  ),

  on(
    MenuActions.incrementPagination,
    (state: MenuState, { menuList }): MenuState => {
      return {
        ...state,
        menuPagination: state.menuPagination + 1,
        menuList: [...state.menuList, ...menuList],
        isLoading: false,
        isError: false,
        triggerGetMenus: true,
      };
    }
  ),

  on(MenuActions.getMenusFail, (state): MenuState => {
    return {
      ...state,
      isLoading: false,
      isError: true,
    };
  }),

  on(MenuActions.getMenusCooker, (state: MenuState): MenuState => {
    return {
      ...state,
      isLoading: true,
      isError: false,
    };
  }),

  on(
    MenuActions.toggleMenuAvailabilitySuccess,
    (state: MenuState, { menu }): MenuState => {
      if (!menu) {
        return state;
      }

      const updatedMenuCookerList = state.menuCookerList.map((m) => {
        if (m.ID === menu.ID) {
          return {
            ...m,
            Open: !m.Open,
          };
        }
        return m;
      });

      return {
        ...state,
        menuCookerList: updatedMenuCookerList,
        isLoading: false,
        isError: false,
      };
    }
  ),

  on(MenuActions.getMenusCookerFail, (state): MenuState => {
    return {
      ...state,
      isLoading: false,
      isError: true,
    };
  }),

  //   on(MenuActions.addMenusFail, (state): MenuState => {
  //     return {
  //       ...state,
  //       isError: true,
  //     };
  //   }),

  //   on(MenuActions.addMenus, (state: MenuState, { newMenu }): MenuState => {
  //     return {
  //       ...state,
  //       menuList: [...state.menuList, newMenu],
  //       isLoading: false,
  //     };
  //   }),

  on(
    MenuActions.addMenuCookerSuccess,
    (state: MenuState, { menu }): MenuState => {
      return {
        ...state,
        menuList: [...state.menuList, menu],
        menuCookerToPost: {
          entreeId: -1,
          mainDishId: -1,
          dessertId: -1,
          description: '',
          partielPrice: 0,
          totalPrice: 0,
        },
        menuCookerList: [...state.menuCookerList, menu],
        isLoading: false,
      };
    }
  ),

  on(MenuActions.addMenuCookerFail, (state): MenuState => {
    return {
      ...state,
      isError: true,
    };
  })
);

export function MenuReducer(state: any, action: Action) {
  return _MenuReducer(state, action);
}
