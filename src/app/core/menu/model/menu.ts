import { Dish } from "app/core/dish/model/dish";
import { User } from "app/core/user/model/user.model";

  export interface Menu extends MenuRequest {
    Open: boolean;
    CookerID: User;
    Entree: Dish;
    MainDish: Dish;
    Dessert: Dish;
    Description: string;
  }

  export interface MenuRequest {
    ID: number;
    Open: boolean;
    EntreeID: number;
    MainDishID: number;
    DessertID: number;
    Description: string;
    PartialPrice: number;
    TotalPrice: number;
  }

  export interface MenuCookerToPost {
    entreeId: number,
    mainDishId: number,
    dessertId: number,
    description: string
    partielPrice: number,
    totalPrice: number
  }


  export interface MenuCommandResponse {
    ID: number;
    CookerID: User;
    Entree: Dish;
    MainDish: Dish;
    Dessert: Dish;
    Open: boolean;
    EntreeID: number;
    MainDishID: number;
    DessertID: number;
    Description: string;
    PartialPrice: number;
    TotalPrice: number;
  }