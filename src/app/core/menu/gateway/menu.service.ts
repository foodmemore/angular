import { Injectable } from '@angular/core';
import { GatewayHelper } from 'app/core/helpers/gateway.helper';
import { Menu, MenuCookerToPost } from '../model/menu';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MenuGatewayService extends GatewayHelper<Menu> {

  constructor() {
    super('/menu');
   }


  public getAllMenus(page: number): Observable<Menu[]> {
    const params = new HttpParams().set('page', page.toString())
    return this.http.get<Menu[]>(this.apiUrl, { headers: this.headers, params: params });
  }


  public getAllMenusCooker(): Observable<Menu[]> {
    return this.http.get<Menu[]>(this.apiUrl + 'cooker', { headers: this.headers });
  }

  public postMenuCooker(newMenu: MenuCookerToPost): Observable<Menu> {
    return this.http.post<Menu>(this.apiUrl , newMenu, { headers: this.headers });
  }

  public toggleMenuAvailability(menuID: number): Observable<Menu>{
    return this.http.patch<Menu>(this.apiUrl + 'toggle/' + menuID, null,{ headers: this.headers });
   }

}
