import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarService } from './snackbar.service'; // Adjust the import path as necessary

describe('SnackbarService', () => {
  let service: SnackbarService;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let snackBarMock: any;


  beforeEach(async () => {
    snackBarMock = {
      open: jest.fn()
    };

    await TestBed.configureTestingModule({
      providers: [
        SnackbarService,
        { provide: MatSnackBar, useValue: snackBarMock }
      ]
    }).compileComponents();

    service = TestBed.inject(SnackbarService);
  });

  it('should call MatSnackBar.open with the correct parameters for showSuccess', () => {
    const message = 'Test Success Message';
    service.showSuccess(message);
    expect(snackBarMock.open).toHaveBeenCalledWith(message, 'Fermer', {
      duration: 2000,
      horizontalPosition: 'end',
    });
  });

  it('should call MatSnackBar.open with the correct parameters for showError', () => {
    const message = 'Test Error Message';
    service.showError(message);
    expect(snackBarMock.open).toHaveBeenCalledWith(message, 'Fermer', {
      duration: 2000,
      horizontalPosition: 'end',
    });
  });
});