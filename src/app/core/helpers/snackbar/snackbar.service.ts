import { Injectable, inject } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  private readonly snackBar: MatSnackBar = inject(MatSnackBar);
  duration: number = 2000;
  position: MatSnackBarHorizontalPosition = 'end';

  showSuccess(message: string): void {
    this.snackBar.open(message, 'Fermer', {
      duration: this.duration,
      horizontalPosition: this.position,
    });
  }

  showError(message: string): void {
    this.snackBar.open(message, 'Fermer', {
      duration: this.duration,
      horizontalPosition: this.position,
    });
  }
}
