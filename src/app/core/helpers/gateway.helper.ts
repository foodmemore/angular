import { HttpHeaders, HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { environment } from 'environments/environment';

import { Observable } from 'rxjs';

export abstract class GatewayHelper<T> {
  protected apiUrl = environment.apiUrl;
  protected headers: HttpHeaders;
  protected headersFormData: HttpHeaders;
  protected http: HttpClient = inject(HttpClient);

  constructor(path?: string) {
    this.apiUrl = environment.apiUrl + (path ? path + '/' : '/');
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    this.headersFormData = new HttpHeaders({
    
    });
  }

  public getOne(id: string | number): Observable<T | T[]> {
    return this.http.get<T[]>(this.apiUrl + id, { headers: this.headers });
  }

  public getAll(): Observable<T[]> {
    return this.http.get<T[]>(this.apiUrl, { headers: this.headers });
  }

  public post(newObj: T): Observable<T> {
    return this.http.post<T>(this.apiUrl, newObj, { headers: this.headers });
  }

  public postFormData(newObj: FormData): Observable<T> {
    return this.http.post<T>(this.apiUrl, newObj, {
      headers: this.headersFormData,
    });
  }

  public update(updateObj: T): Observable<T> {
    return this.http.put<T>(this.apiUrl, updateObj, { headers: this.headers });
  }

  public updateFormData(updateObj: FormData): Observable<T> {
    return this.http.put<T>(this.apiUrl, updateObj, {
      headers: this.headersFormData,
    });
  }

  public delete(id: string): Observable<T> {
    return this.http.delete<T>(this.apiUrl + id, { headers: this.headers });
  }
}
