import { TestBed } from '@angular/core/testing';
import { CanActivateChildFn } from '@angular/router';

import { isCookerGuard } from './is-cooker.guard';

describe('isCookerGuard', () => {
  const executeGuard: CanActivateChildFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => isCookerGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
