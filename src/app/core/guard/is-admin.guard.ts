import { inject } from '@angular/core';
import { CanActivateChildFn, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectCurrentUseRoleState } from '../user/store/user.selector';
import { UserCurrentUseRole } from '../user/store/user.state';
import { firstValueFrom, map } from 'rxjs';
import { SnackbarService } from '../helpers/snackbar/snackbar.service';

export const isAdminGuard: CanActivateChildFn = async () => {

  const store: Store = inject(Store);
  const router = inject(Router)
  const snackBar = inject(SnackbarService)

  const isAdminGuard = await firstValueFrom(
    store.select(selectCurrentUseRoleState).pipe(
      map(userState => {
        if (userState !== UserCurrentUseRole.ADMIN) {
          snackBar.showError('Erreur inconnue');
          router.navigate(['/profile']);
          return false;
        }
        return true;
      })
    )
  );
  return isAdminGuard;
};