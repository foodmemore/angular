import { inject } from '@angular/core';
import { CanActivateChildFn, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectCurrentUseRoleState } from '../user/store/user.selector';
import { UserCurrentUseRole } from '../user/store/user.state';
import { firstValueFrom, map } from 'rxjs';

export const isLoggedGuard: CanActivateChildFn = async () => {

  const store: Store = inject(Store);
  const router = inject(Router)
  
  const isLoggedIn = await firstValueFrom(
    store.select(selectCurrentUseRoleState).pipe(
      map(userState => {
        if (userState === UserCurrentUseRole.NOT_LOGGED) {
          router.navigate(['/login']);
          return false;
        }
        return true;
      })
    )
  );
   return isLoggedIn;
};