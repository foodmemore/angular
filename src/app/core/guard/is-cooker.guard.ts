import { inject } from '@angular/core';
import { CanActivateChildFn, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectCurrentUseRoleState } from '../user/store/user.selector';
import { UserCurrentUseRole } from '../user/store/user.state';
import { firstValueFrom, map } from 'rxjs';
import { SnackbarService } from '../helpers/snackbar/snackbar.service';

export const isCookerGuard: CanActivateChildFn = async () => {

  const store: Store = inject(Store);
  const router = inject(Router)
  const snackBar = inject(SnackbarService)

  const isCookerGuard = await firstValueFrom(
    store.select(selectCurrentUseRoleState).pipe(
      map(userState => {
        if (userState !== UserCurrentUseRole.COOKER) {
          snackBar.showError('Vous devez être un.e cuisinier.e pour accéder à cette page, faites votre demande ici');
          router.navigate(['/profile']);
          return false;
        }
        return true;
      })
    )
  );
  return isCookerGuard;
};