import { UserCurrentUseRole } from "../store/user.state";

export type User = {
  id: number;
  firstName: string;
  lastName: string;
  roles: string[];
  email: string;
  password: string;
  profilePicture: string;
  isValidLicense: Date | string;
  addressStreet: string;
  addressCity: string;
  addressZipCode: string;
  currentUseRole: UserCurrentUseRole;
};
