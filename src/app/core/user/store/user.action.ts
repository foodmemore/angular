import { createAction, props } from '@ngrx/store';

import { User } from '../model/user.model';
import { LoginDto } from 'app/core/auth/dto/login.dto';
import { RegisterDto } from 'app/core/auth/dto/register.dto';
import { UserCurrentUseRole } from './user.state';

//login
export const LOGIN_LOADING = '[user] login loading';
export const LOGIN_SUCCESS = '[user] login success';
export const LOGIN_FAIL = '[user] login fail';

export const login = createAction(LOGIN_LOADING, props<{ login: LoginDto }>());
export const loginSuccess = createAction(
  LOGIN_SUCCESS,
  props<{ user: User }>()
);
export const loginFail = createAction(LOGIN_FAIL, props<{ error: string }>());

export const REFRESH_TOKENS = '[user] refresh tokens';
export const REFRESH_TOKENS_SUCCESS = '[user] refresh tokens success';
export const REFRESH_TOKENS_FAIL = '[user] refresh tokens fail';
export const refreshTokens = createAction(REFRESH_TOKENS);
export const refreshTokensSuccess = createAction(
  REFRESH_TOKENS_SUCCESS,
  props<{ user: User }>()
);
export const refreshTokensFail = createAction(REFRESH_TOKENS_FAIL);

export const LOGOUT = '[user] logout';
export const LOGOUT_SUCCESS = '[user] logout success';
export const LOGOUT_FAILURE = '[user] logout fail';
export const logout = createAction(LOGOUT);
export const logoutSuccess = createAction(LOGOUT_SUCCESS);
export const logoutFailure = createAction(
  LOGOUT_FAILURE,
  props<{ error: string }>()
);

export const REGISTER_LOADING = '[user] register loading';
export const REGISTER_SUCCESS = '[user] register success';
export const REGISTER_FAILURE = '[user] register fail';
export const register = createAction(
  REGISTER_LOADING,
  props<{ register: RegisterDto }>()
);
export const registerSuccess = createAction(
  REGISTER_SUCCESS,
  props<{ user: User }>()
);
export const registerFailure = createAction(
  REGISTER_FAILURE,
  props<{ error: string }>()
);

// update
export const UPDATE_USER_USE_ROLE_LOADING = '[user] update user loading';
export const UPDATE_USER_USE_ROLE_SUCCESS = '[user] update user success';
export const UPDATE_USER_USE_ROLE_FAILURE = '[user] update user fail';
export const updateUserUseRole = createAction(
  UPDATE_USER_USE_ROLE_LOADING,
  props<{ useCurrentRole: UserCurrentUseRole }>()
);
export const updateUserUseRoleSuccess = createAction(
  UPDATE_USER_USE_ROLE_SUCCESS,
  props<{ useCurrentRole: UserCurrentUseRole }>()
);
export const updateUserUseRoleFailure = createAction(
  UPDATE_USER_USE_ROLE_FAILURE,
  props<{ error: string }>()
);
