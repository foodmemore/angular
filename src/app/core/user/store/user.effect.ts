/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { AuthenticationGatewayService } from 'app/core/auth/gateway/gateway.service';
import * as UserActions from './user.action';
import { catchError, filter, map, of, switchMap, tap } from 'rxjs';
import { LoginDto } from 'app/core/auth/dto/login.dto';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';
import { selectCurrentUseRoleState } from './user.selector';
import { UserCurrentUseRole } from './user.state';
import { Router } from '@angular/router';
import { RegisterDto } from 'app/core/auth/dto/register.dto';
import { UserGatewayService } from '../gateway/user.gateway';

interface LoginLoadingAction {
  type: typeof UserActions.LOGIN_LOADING;
  login: LoginDto;
}

interface RegisterLoadingAction {
  type: typeof UserActions.REGISTER_LOADING;
  register: RegisterDto;
}


interface UpdateUserUseRoleLoadingAction {
  type: typeof UserActions.UPDATE_USER_USE_ROLE_LOADING;
  useCurrentRole: UserCurrentUseRole;
}

@Injectable()
export class UserEffect {
  private readonly store: Store = inject(Store);
  private readonly actions$: Actions = inject(Actions);
  private readonly authenticationGatewayService: AuthenticationGatewayService =
    inject(AuthenticationGatewayService);
  private readonly userGatewayService: UserGatewayService = inject(UserGatewayService); 
  private readonly snackBar: SnackbarService = inject(SnackbarService);
  private readonly router: Router = inject(Router);

  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.LOGIN_LOADING),
      switchMap((action: LoginLoadingAction) =>
        this.authenticationGatewayService.login(action.login).pipe(
          map((user) => UserActions.loginSuccess({ user })),
          catchError(() =>
            of(UserActions.loginFail({ error: 'Erreur connexion' }))
          )
        )
      )
    );
  });

  loginSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(UserActions.loginSuccess),
        concatLatestFrom(() => this.store.select(selectCurrentUseRoleState)),
        map(([a, roleState]) => {
          this.snackBar.showSuccess('Connexion réussie');
          if (roleState === UserCurrentUseRole.USER) {
            this.router.navigate(['/menu']);
          }
          if (roleState === UserCurrentUseRole.SUPER_ADMIN) {
            this.router.navigate(['/admin']);
          }
          if (roleState === UserCurrentUseRole.ADMIN) {
            this.router.navigate(['/admin']);
          }
          if (roleState === UserCurrentUseRole.COOKER) {
            this.router.navigate(['/preparation']);
          }
        })
      );
    },
    { dispatch: false }
  );

  loginFail$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(UserActions.loginFail),
        map(() => {
          this.snackBar.showError('Erreur connexion');
        })
      );
    },
    { dispatch: false }
  );

  refreshTokens$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.REFRESH_TOKENS),
      switchMap(() =>
        this.authenticationGatewayService.refreshToken().pipe(
          map((user) => UserActions.refreshTokensSuccess({ user })),
          catchError(() =>
            of(UserActions.loginFail({ error: 'Erreur connexion' }))
          )
        )
      )
    );
  });

  logout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.logout),
      switchMap(() => 
        this.authenticationGatewayService.logout().pipe(
          map(() => UserActions.logoutSuccess()),
          tap(() => this.router.navigate(['/menu'])),
          tap(() => this.snackBar.showSuccess('Déconnexion Réussie')),
          catchError(error => of(UserActions.logoutFailure({ error }))) // Handle potential errors
        )
      )
    );
  });

  refreshTokensSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.refreshTokensSuccess),
      concatLatestFrom(() => this.store.select(selectCurrentUseRoleState)),
      filter(
        ([userAction, roleState]) => roleState === UserCurrentUseRole.NOT_LOGGED
      ),
      map(([userAction]) => UserActions.loginSuccess({ user: userAction.user }))
    );
  });

  register$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.REGISTER_LOADING),
      switchMap((action: RegisterLoadingAction) =>
        this.authenticationGatewayService.register(action.register).pipe(
          map((user) => UserActions.registerSuccess({ user })),
          catchError(() =>
            of(UserActions.registerFailure({ error: 'Erreur inscription' }))
          )
        )
      )
    );
  });
  
  registerSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(UserActions.registerSuccess),
        tap(() => {
          this.snackBar.showSuccess('Inscription réussie');
          this.router.navigate(['/']);
        })
      );
    },
    { dispatch: false }
  );
  
  registerFail$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(UserActions.registerFailure),
        tap(() => {
          this.snackBar.showError('Erreur inscription');
        })
      );
    },
    { dispatch: false }
  );

  updateUserUseRole$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.UPDATE_USER_USE_ROLE_LOADING),
      switchMap((action: UpdateUserUseRoleLoadingAction) =>
        this.userGatewayService.changeUserRole(action.useCurrentRole).pipe(
          map(() => UserActions.updateUserUseRoleSuccess({ useCurrentRole: action.useCurrentRole })),
          catchError(() =>
            of(UserActions.updateUserUseRoleFailure({ error: 'Erreur mise à jour' }))
          )
        )
      )
    );
  });

  updateUserUseRoleSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.updateUserUseRoleSuccess),
      tap(() => {
        this.snackBar.showSuccess('Mise à jour spécifié');
      })
    );
  }, { dispatch: false });

  updateUserUseRoleFailure$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.updateUserUseRoleFailure),
      tap(() => {
        this.snackBar.showError('Erreur mise à jour');
      })
    );
  }, { dispatch: false });
}
