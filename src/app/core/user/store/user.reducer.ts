/* eslint-disable @typescript-eslint/no-explicit-any */
import { Action, createReducer, on } from '@ngrx/store';
import { UserCurrentUseRole, UserState, userState } from './user.state';
import * as UserActions from './user.action';
import { User } from '../model/user.model';


const _UserReducer = createReducer<UserState>(
  userState,

  on(UserActions.loginSuccess, (state, { user }): UserState => {
    return {
      ...state,
      user: user,
      loading: false,
      error: false,
      redirect: true,
    };
  }),

  on(UserActions.loginFail, (state): UserState => {
    return {
      ...state,
      loading: false,
      error: true,
    };
  }),

  on(UserActions.logoutSuccess, (state): UserState => {
    const newUserState: User = {
      id: -1,
      firstName: '',
      lastName: '',
      roles: [],
      email: '',
      password: '',
      profilePicture: '',
      isValidLicense: '',
      addressStreet: '',
      addressCity: '',
      addressZipCode: '',
      currentUseRole: UserCurrentUseRole.NOT_LOGGED,
    };

    return {
      ...state,
      user: newUserState,
      loading: false,
      error: false,
      redirect: true,
    };
  }),

  on(UserActions.registerSuccess, (state, { user }): UserState => {
    return {
      ...state,
      user: user,
      loading: false,
      redirect: true,
    };
  }),

  on(UserActions.refreshTokensSuccess, (state, { user }): UserState => {
    return {
      ...state,
      user: user,
      loading: false,
      error: false,
      redirect: true,
    };
  }),
  
  on(UserActions.updateUserUseRoleSuccess, (state, { useCurrentRole }): UserState => {
    const updatedUser = {
      ...state.user,
      currentUseRole: useCurrentRole
    };

    return {
      ...state,
      user: updatedUser,
    };
  })
);
export function UserReducer(state: any, action: Action) {
  return _UserReducer(state, action);
}
