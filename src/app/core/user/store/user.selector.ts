import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserCurrentUseRole, UserState } from './user.state';

const selectUserState = createFeatureSelector<UserState>('user');

export const selectCurrentUserState = createSelector(
  selectUserState,
  (state: UserState) => state.user
);
export const selectCurrentUseRoleState = createSelector(
  selectUserState,
  (state: UserState) => state.user.currentUseRole ?? UserCurrentUseRole.NOT_LOGGED
);
