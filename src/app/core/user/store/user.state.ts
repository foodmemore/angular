import { User } from '../model/user.model';

export enum UserCurrentUseRole {
  USER = 'USER',
  ADMIN = 'ADMIN',
  COOKER = 'COOKER',
  SUPER_ADMIN = 'SUPER_ADMIN',
  NOT_LOGGED = 'NOT_LOGGED',
}

export interface UserState {
  user: User;
  loading: boolean;
  error: boolean;
  redirect: boolean;
}

export const userState: UserState = {
  user: {
    id: -1,
    firstName: '',
    lastName: '',
    roles: [],
    email: '',
    password: '',
    profilePicture: '',
    isValidLicense: '',
    addressStreet: '',
    addressCity: '',
    addressZipCode: '',
    currentUseRole: UserCurrentUseRole.NOT_LOGGED,
  },

  loading: false,
  error: false,
  redirect: false,
};
