/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { GatewayHelper } from 'app/core/helpers/gateway.helper';
import { User } from '../model/user.model';
import { UserCurrentUseRole } from '../store/user.state';
import { Observable } from 'rxjs';

export interface UserLicenseRequest {
  accept: boolean;

  user_id: number;
}

@Injectable({
  providedIn: 'root',
})
export class UserGatewayService extends GatewayHelper<User> {
  constructor() {
    super('/user');
  }

  public changeUserRole(role: UserCurrentUseRole): Observable<User> {
    return this.http.patch<User>(
      this.apiUrl + 'role',
      { role },
      { headers: this.headers }
    );
  }

  public askToBecomeCooker(): Observable<User> {
    return this.http.patch<User>(
      this.apiUrl + 'ask-to-become-cooker',
      {},
      { headers: this.headers }
    );
  }

  public adminAdeptUserBecomeCooker(
    userLicenseRequest: UserLicenseRequest
  ): Observable<User> {
    return this.http.patch<User>(this.apiUrl + 'license', userLicenseRequest, {
      headers: this.headers,
    });
  }
}
