/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { GatewayHelper } from 'app/core/helpers/gateway.helper';
import { Observable } from 'rxjs';
interface IresponseStripe {
  clientSecret: string;
}

@Injectable({
  providedIn: 'root',
})
export class PaymentGatewayService extends GatewayHelper<any> {
  constructor() {
    super('/payment');
  }

  public createStripePaymentIntent(amount: number): Observable<IresponseStripe> {
    return this.http.post<IresponseStripe>(this.apiUrl + 'create', {amount: amount, currency: 'EUR'}, { headers: this.headers });
  }

}
