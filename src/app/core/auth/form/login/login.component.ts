import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
  input,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule, RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { login } from '../../../../core/user/store/user.action';
import { AuthenticationGatewayService } from '../../gateway/gateway.service';
import { DebounceDirective } from 'app/shared/directives/debounce.directive';

@Component({
  selector: 'app-form-login',
  standalone: true,
  providers: [DebounceDirective],
  imports: [
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    RouterOutlet,
    RouterModule,

  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  emailInput = input<string>();
  constructor(private readonly store: Store) {}

  readonly authenticationGatewayService: AuthenticationGatewayService = inject(
    AuthenticationGatewayService
  );

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(this.emailInput(), [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  onSubmit() {

    if (this.loginForm.valid) {
      this.loggedIn();
    }
  }

  loggedIn() {
    this.store.dispatch(
      login({
        login: {
          email: this.loginForm.get('email')?.value,
          password: this.loginForm.get('password')?.value,
        },
      })
    );
  }



}
