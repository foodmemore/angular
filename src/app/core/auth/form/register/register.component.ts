import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIcon } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { register } from 'app/core/user/store/user.action';
import { RegisterDto } from '../../dto/register.dto';

@Component({
  selector: 'app-form-register',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIcon,
    RouterOutlet,
  ],
  template: `
    <form [formGroup]="registerForm" (ngSubmit)="onSubmit()">
      <!-- Email -->
      <mat-form-field>
        <mat-label>Email</mat-label>
        <input
          type="email"
          matInput
          formControlName="email"
          placeholder="Ex. pat@example.com"
        />
        @if(registerForm.get('email')?.hasError('email')) {
        <mat-error>Veuillez entrer une adresse email valide</mat-error>
        } @if(registerForm.get('email')?.hasError('required')) {
        <mat-error>L'adresse email est <strong>requise</strong></mat-error>
        }
      </mat-form-field>
      <!-- Prénom -->
      <mat-form-field>
        <mat-label>Prénom</mat-label>
        <input
          type="text"
          matInput
          minlength="3"
          maxlength="100"
          formControlName="first_name"
          placeholder="Ex. John"
        />
        @if(registerForm.get('first_name')?.hasError('required')) {
        <mat-error>Le prénom est <strong>requise</strong></mat-error>
        } @if(registerForm.get('first_name')?.hasError('minlength')) {
        <mat-error
          >Le prénom doit avoir au moins
          <strong>3 caractères</strong></mat-error
        >
        } @if(registerForm.get('first_name')?.hasError('maxlength')) {
        <mat-error
          >Le prénom doit avoir au plus
          <strong>100 caractères</strong></mat-error
        >
        }
      </mat-form-field>
      <!-- Nom -->
      <mat-form-field>
        <mat-label>Nom</mat-label>
        <input
          type="text"
          matInput
          minlength="3"
          maxlength="100"
          formControlName="last_name"
          placeholder="Ex. Doe"
        />
        @if(registerForm.get('last_name')?.hasError('required')) {
        <mat-error>Le nom est <strong>requise</strong></mat-error>
        } @if(registerForm.get('last_name')?.hasError('minlength')) {
        <mat-error
          >Le nom doit avoir au moins <strong>3 caractères</strong></mat-error
        >
        } @if(registerForm.get('last_name')?.hasError('maxlength')) {
        <mat-error
          >Le nom doit avoir au plus <strong>100 caractères</strong></mat-error
        >
        }
      </mat-form-field>
      <!-- Mot de passe -->
      <mat-form-field>
        <mat-label>Mot de passe</mat-label>
        <input
          [type]="hidePassword ? 'password' : 'text'"
          matInput
          formControlName="password"
        />
        <button mat-icon-button matSuffix (click)="togglePasswordVisibility()">
          <mat-icon>{{
            hidePassword ? 'visibility_off' : 'visibility'
          }}</mat-icon>
        </button>
        @if(registerForm.get('password')?.hasError('required')) {
        <mat-error>Mot de passe <strong>requis</strong></mat-error>
        }
      </mat-form-field>
      <!-- Adresse -->
      <mat-form-field>
        <mat-label>Adresse</mat-label>
        <input
          type="text"
          matInput
          formControlName="address_street"
          placeholder="Ex. 10 rue de la paix"
        />
      </mat-form-field>
      <mat-form-field>
        <mat-label>Ville</mat-label>
        <input
          type="text"
          matInput
          formControlName="address_city"
          placeholder="Ex. Paris"
        />
      </mat-form-field>
      <mat-form-field>
        <mat-label>Code postal</mat-label>
        <input
          type="text"
          matInput
          formControlName="address_zip"
          placeholder="Ex. 75000"
        />
      </mat-form-field>
      <ng-content>
        
      </ng-content>
      
      <!-- BOUTON -->
      <button [disabled]="!registerForm.valid" mat-raised-button color="primary">
        S'enregistrer
      </button>
    </form>
  `,
  styleUrl: './register.component.scss',
})
export class RegisterComponent implements OnInit {
  // readonly store: Store = inject(Store);
  constructor(private readonly store: Store) {}
  hidePassword: boolean = true;


  registerForm!: FormGroup;

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      first_name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100),
      ]),
      last_name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100),
      ]),
      address_street: new FormControl(''),
      address_city: new FormControl(''),
      address_zip: new FormControl(''),
    });
  }

  onSubmit() {
    if (this.registerForm.valid) {
      const registerDto: RegisterDto = {
        email: this.registerForm.get('email')?.value,
        password: this.registerForm.get('password')?.value,
        first_name: this.registerForm.get('first_name')?.value,
        last_name: this.registerForm.get('last_name')?.value,
        address_street: this.registerForm.get('address_street')?.value,
        address_city: this.registerForm.get('address_city')?.value,
        address_zip: this.registerForm.get('address_zip')?.value,
      };
      this.registerIn(registerDto);

      this.registerForm.reset();

    }
  }

  togglePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  registerIn(registerDto: RegisterDto) {
    this.store.dispatch(
      register({
        register: {
          ...registerDto,
        },
      })
    );
  }
}
