/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { GatewayHelper } from 'app/core/helpers/gateway.helper';
import { User } from 'app/core/user/model/user.model';
import { LoginDto } from '../dto/login.dto';
import { Observable } from 'rxjs';
import { RegisterDto } from '../dto/register.dto';

export interface RegisterEmailPassword {
  email: string;
  password: string;
}

export interface RegisterToken {
  email: string;
  password: string;
  token: string;
}
export interface RegisterFinalUpdate {
  email: string;
  password: string;
  first_name: string;
  last_name: string;
  address_street: string;
  address_city: string;
  address_zip_code: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationGatewayService extends GatewayHelper<User> {
  constructor() {
    super('/auth');
  }

  public login(loginUser: LoginDto): Observable<User> {
    return this.http.post<User>(
      this.apiUrl + 'login',
      loginUser,
      {
        withCredentials: true
      }
    );
  }

  public register(registerUser: RegisterDto): Observable<User> {
    return this.http.post<User>(this.apiUrl + 'register', registerUser, {
      headers: this.headers,
    });
  }

  public validate(): Observable<User> {
    return this.http.get<User>(this.apiUrl + 'validate', {
      headers: this.headers,
    });
  }

  public refreshToken(): Observable<User> {
    return this.http.get<User>(this.apiUrl + 'refresh', {
      headers: this.headers,
    });
  }

  public logout(): Observable<unknown> {
    return this.http.get<unknown>(this.apiUrl + 'logout', {
      headers: this.headers,
    });
  }


  public registerVerifyMail(registerCredentials:RegisterEmailPassword): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'register/verify-mail', registerCredentials , {
      headers: this.headers,
    });
  }

  public registerVerifyToken(registerToken:RegisterToken): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'register/verify-token',  registerToken , {
      headers: this.headers,
    });
  }
  // /register/final-update

  public registerFinalUpdate(registerFinalUpdate: RegisterFinalUpdate): Observable<any> { 
    return this.http.patch<any>(this.apiUrl + 'register/final-update', registerFinalUpdate , {
      
    })
  }

  public sendToken(email: string): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'register/send-token', { email }, {
      headers: this.headers,
    });
  }
}
