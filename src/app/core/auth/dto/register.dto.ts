export type RegisterDto = {
    email: string;
    password: string;
    first_name: string;
    last_name: string;
    address_street: string;
    address_city: string;
    address_zip: string;
}