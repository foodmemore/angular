import { Component, inject, OnInit } from '@angular/core';

import { MatTabsModule } from '@angular/material/tabs';
import { DishLayoutComponent } from './dish/dish-layout/dish-layout.component';
import { MenuLayoutComponent } from './menu/menu-layout/menu-layout.component';
import { Store } from '@ngrx/store';
import { getDishes } from 'app/core/dish/store/dish.action';

@Component({
  selector: 'app-preparation-route',
  standalone: true,
  template: `
  
    <mat-tab-group class="nav" animationDuration="100ms">
      <mat-tab label="Menu">
        <div class="child">
          @defer {
          <app-menu-layout />
          }
        </div>
      </mat-tab>
      <mat-tab label="Plats">
        <div class="child">
          @defer() {
          <app-dish-layout />

          }
        </div>
      </mat-tab>
    </mat-tab-group>
  `,
  styles: `
    @import "wrap";
    @import "responsive";
  .child {
    max-width: 1400px;
    margin: 0 auto;
    padding: 0 2svw;
    @include breakpoints(bigPhone) {  
      width: 100svw;
      padding:0;
    }
  }
  .nav {
    margin-bottom: 52px;

  }


  `,
  imports: [MatTabsModule, DishLayoutComponent, MenuLayoutComponent],
})
export class PreparationListRouteComponent implements OnInit {
  readonly store = inject(Store);
  ngOnInit(): void {
    this.store.dispatch(getDishes());


  }

}
