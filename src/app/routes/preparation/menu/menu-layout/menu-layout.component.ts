import {
  ChangeDetectionStrategy,
  Component,
  inject,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { selectMenuCookerListState } from 'app/core/menu/store/menu.selector';
import { MenuListComponent } from '../../../menu/components/list/menu-list/menu-list.component';
import { AsyncPipe, JsonPipe } from '@angular/common';
import { MenuListLoadingComponent } from '../../../menu/components/list/menu-list-loading/menu-list-loading.component';
import { FormMenuComponent } from '../../components/form-menu/form-menu.component';
import { DishType } from 'app/core/dish/enum/dish-type.enum';
import { Dish } from 'app/core/dish/model/dish';
import { selectDishListState } from 'app/core/dish/store/dish.selector';
import { Observable } from 'rxjs';
import { MatButton } from '@angular/material/button';
import { getMenusCooker } from 'app/core/menu/store/menu.action';
import { MenuListCookerComponent } from '../../../menu/components/list/menu-list/menu-list-cooker.component';

@Component({
  selector: 'app-menu-layout',
  standalone: true,
  imports: [
    MatButton,
    MenuListComponent,
    AsyncPipe,
    MenuListLoadingComponent,
    FormMenuComponent,
    JsonPipe,
    MenuListCookerComponent,
  ],
  template: `
    <section>
      <h2>Ajouter un menu</h2>

      @if(dishList$ | async; as dishList) { @if(dishList.length > 0) {
      @if(isAddingMenu) {
      <button (click)="isAddingMenu = false" type="button" mat-raised-button>
        Annuler
      </button>
      <app-form-menu
        (outIsAddingMenu)="isAddingMenu = $event"
        [dishList]="dishList"
      />
      } @else {
      <button
        (click)="isAddingMenu = true"
        type="button"
        mat-raised-button
        color="primary"
      >
        Ajouter un nouveau menu
      </button>
      } } }
      <h2>Liste de vos menus</h2>

      @if(menuCookerList$ | async; as menuCookerList) {

      <app-menu-list-cooker [menuList]="menuCookerList" />
      } @else {
      <app-menu-list-loading />
      }
    </section>
  `,
  styles: `
  @import "button";
  section {
  padding: 1rem;
}
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuLayoutComponent implements OnInit {
  ngOnInit(): void {
    this.store.dispatch(getMenusCooker());
  }

  dishType = DishType;
  store: Store = inject(Store);
  dishList$: Observable<Dish[]> = this.store.select(selectDishListState);
  isAddingMenu = false;
  menuCookerList$ = this.store.select(selectMenuCookerListState);
}
