import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
} from '@angular/core';
import { DishAddComponent } from '../../components/dish-add/dish-add.component';
import { DishListComponent } from '../../components/dish-list/dish-list.component';
import { FormDishComponent } from '../../components/form-dish/form-dish.component';
import { DishType } from 'app/core/dish/enum/dish-type.enum';
import { Store } from '@ngrx/store';
import { Dish } from 'app/core/dish/model/dish';
import { setCurrentState } from 'app/core/dish/store/dish.action';
import { Observable } from 'rxjs';
import { AsyncPipe, JsonPipe } from '@angular/common';
import {
  selectDishCurrentState,
  selectDishListState,
} from 'app/core/dish/store/dish.selector';

@Component({
  selector: 'app-dish-layout',
  standalone: true,
  template: `
    <section>
      <h2>Ajouter un plat</h2>

      <app-dish-add (currentStateOutPut)="setCurrentState($event)" />
      @if(currentState$ | async; as currentState) { @if(currentState !==
      dishType.NotActive) {
      <app-form-dish [type]="currentState" />
      } @else {
      <h2 class="wrap">Mes plats</h2>

      @if(dishList$ | async; as dishList) {

      <app-dish-list [dishList]="dishList" />

      } @else {
      <p>Chargement</p>
      } } }
    </section>
  `,
  styles: `
section {
  padding: 1rem;
}
  `,
  changeDetection: ChangeDetectionStrategy.Default,
  imports: [
    DishAddComponent,
    DishListComponent,
    FormDishComponent,
    AsyncPipe,
    JsonPipe,
  ],
})
export class DishLayoutComponent implements OnInit, AfterViewInit {
  dishType = DishType;
  currentState$!: Observable<DishType | undefined>;
  readonly store: Store = inject(Store);
  dishList$!: Observable<Dish[]>;

  ngOnInit(): void {
    this.currentState$ = this.store.select(selectDishCurrentState);
    this.dishList$ = this.store.select(selectDishListState);
  }

  ngAfterViewInit(): void {
    this.dishList$ = this.store.select(selectDishListState);
  }

  setCurrentState(state: DishType) {
    this.store.dispatch(setCurrentState({ currentState: state }));
  }
}
