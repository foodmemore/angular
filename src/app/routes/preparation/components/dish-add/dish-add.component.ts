import {
  ChangeDetectionStrategy,
  Component,
  OutputEmitterRef,
  output,
} from '@angular/core';
import { DishAddCardComponent } from '../dish-add-card/dish-add-card.component';
import { DishType } from 'app/core/dish/enum/dish-type.enum';

@Component({
  selector: 'app-dish-add',
  standalone: true,
  template: `
    <section class="cards">
      <app-dish-add-card
        (click)="toggleDishType(dishType.Entree)"
        [type]="dishType.Entree"
        [isActive]="currentState === dishType.Entree"
      />
      <app-dish-add-card
        (click)="toggleDishType(dishType.MainDish)"
        [type]="dishType.MainDish"
        [isActive]="currentState === dishType.MainDish"
      />
      <app-dish-add-card
        (click)="toggleDishType(dishType.Dessert)"
        [type]="dishType.Dessert"
        [isActive]="currentState === dishType.Dessert"
      />
    </section>
  `,
  styles: `
@import "responsive";    
.cards {
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
}
    `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [DishAddCardComponent],
})
export class DishAddComponent {
  dishType = DishType;
  currentStateOutPut: OutputEmitterRef<DishType> = output<DishType>();
  currentState?: DishType;
  toggleDishType(dishType: DishType) {
    if (this.currentState === dishType) {
      this.currentState = this.dishType.NotActive;
      this.currentStateOutPut.emit(this.dishType.NotActive);
    } else {
      this.currentState = dishType;
      this.currentStateOutPut.emit(dishType);
    }
  }
}
