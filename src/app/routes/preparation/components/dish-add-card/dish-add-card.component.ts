import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faIceCream,
  faEgg,
  faUtensils,
} from '@fortawesome/free-solid-svg-icons';
import { DishType } from 'app/core/dish/enum/dish-type.enum';

@Component({
  selector: 'app-dish-add-card',
  standalone: true,
  imports: [FontAwesomeModule],
  template: `
    <div [className]="isActive ? 'card active' : 'card'">
      <fa-icon [icon]="faIcon"></fa-icon>
      <p>{{ text }}</p>
    </div>
  `,
  styles: `
@import "color";
@import "responsive";

.card {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: $borderRadius;
  height: 25svw;
  width: 25svw;
  border: $border;
  max-width: 180px;
  max-height: 180px;
  min-width: 120px;
  min-height: 120px;
  margin: 0.5rem 1rem;
  padding: 0.5rem;
  &.active {
    fa-icon {
      color: $primaryColorLight;
    }
    p {
      color: $primaryColorLight;
    }
  }
  fa-icon {
    color: $secondFontColor;
    font-size: 60px;
    margin-bottom: 0.5rem;

  @include breakpoints(tinyTablet, min) {
    font-size: 100px;
    margin-bottom: 1rem;
  }

  }
  &:hover {
    box-shadow: $boxShadow;
  }

  @include breakpoints(phone) {
    margin: 0.2rem;
  }
}  
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishAddCardComponent implements OnInit {
  dishType = DishType;
  @Input()
  type = '';
  @Input() isActive = false;

  text = 'chargement';
  faIcon = faIceCream;

  ngOnInit(): void {
    switch (this.type) {
      case this.dishType.MainDish:
        this.text = 'Plats';
        this.faIcon = faUtensils;
        break;
      case this.dishType.Entree:
        this.text = 'Entree';
        this.faIcon = faEgg;
        break;
      case this.dishType.Dessert:
        this.text = 'Desserts';
        this.faIcon = faIceCream;
        break;
      default:
        this.text = 'Chargement';
        break;
    }
  }

  faIceCream = faIceCream;
  faEgg = faEgg;
  faUtensils = faUtensils;
}
