import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
} from '@angular/core';
import { DishAddCardComponent } from '../dish-add-card/dish-add-card.component';
import { AsyncPipe, JsonPipe } from '@angular/common';
import { Dish } from 'app/core/dish/model/dish';
import { DishCardComponent } from '../dish-card/dish-card.component';
import { DishType } from 'app/core/dish/enum/dish-type.enum';

@Component({
  selector: 'app-dish-list',
  standalone: true,
  template: `
    <h3>Entrée ({{ dishEntree().length }})</h3>
    @if(dishEntree().length > 0) {
    <section class="card-list">
      @for(entree of dishEntree(); track entree) {
      <app-dish-card [dish]="entree" />
      }
    </section>
    } @else {
    <p>Vous n'avez pas encore de plat</p>
    }

    <h3>Plat ({{ dishMainDish().length }})</h3>
    @if(dishMainDish().length > 0) {

    <section class="card-list">
      @for(main of dishMainDish(); track main) {
      <app-dish-card [dish]="main" />
      }
    </section>

    } @else {
    <p>Vous n'avez pas encore de plat</p>
    }

    <h3>Dessert ({{ dishDessert().length }})</h3>
    @if(dishDessert().length > 0) {
    <section class="card-list">
      @for(dessert of dishDessert(); track dessert) {
      <app-dish-card [dish]="dessert" />
      }
    </section>
    } @else {
    <p>Vous n'avez pas encore de dessert</p>
    }
  `,
  styles: `
  @import 'card'
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [DishAddCardComponent, AsyncPipe, JsonPipe, DishCardComponent],
})
export class DishListComponent {
  dishType = DishType;
  dishList = input.required<Dish[]>();
  dishEntree = computed(() =>
    this.dishList().filter((dish: Dish) => dish.Type === this.dishType.Entree)
  );
  dishMainDish = computed(() =>
    this.dishList().filter((dish: Dish) => dish.Type === this.dishType.MainDish)
  );
  dishDessert = computed(() =>
    this.dishList().filter((dish: Dish) => dish.Type === this.dishType.Dessert)
  );
}
