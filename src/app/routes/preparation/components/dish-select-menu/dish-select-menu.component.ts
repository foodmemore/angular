import { JsonPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  input,
  output,
} from '@angular/core';
import { Dish } from 'app/core/dish/model/dish';
import { DishCardComponent } from '../dish-card/dish-card.component';

@Component({
  selector: 'app-dish-select-menu',
  standalone: true,
  imports: [JsonPipe, DishCardComponent],
  template: `
    <!-- {{dishList() | json}} -->
    <section class="card-list-row">
      @for (dish of dishList(); track $index) {
      <!-- <img src="{{dish.ImgSrc}}" alt="{{dish.Title}}">
    <h2>{{dish.Title}}</h2> -->
      <app-dish-card
        (click)="selectDish(dish.ID)"
        [isInactive]="!isDishSelected(dish.ID)"
        [isSelected]="isDishSelected(dish.ID)"
        [dish]="dish"
      />
      }
    </section>
  `,
  styles: `
  @import 'card'
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishSelectMenuComponent {
  dishList = input.required<Dish[]>();
  idDishOutPut = output<number>();

  idDishSelected!: number;

  isDishSelected(id: number): boolean {
    return this.idDishSelected === id;
  }

  selectDish(id: number) {
    this.idDishSelected = id;
    this.idDishOutPut.emit(id);
  }
}
