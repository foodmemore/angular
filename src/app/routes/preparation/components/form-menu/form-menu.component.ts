import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
  OnInit,
  output,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DishType } from 'app/core/dish/enum/dish-type.enum';
import { Dish } from 'app/core/dish/model/dish';
import { DishSelectMenuComponent } from '../dish-select-menu/dish-select-menu.component';
import { JsonPipe } from '@angular/common';
import { MatButton } from '@angular/material/button';
import { MenuCookerToPost } from 'app/core/menu/model/menu';
import { Store } from '@ngrx/store';
import { addMenuCooker } from 'app/core/menu/store/menu.action';
import { MatStepperModule } from '@angular/material/stepper';

@Component({
  selector: 'app-form-menu',
  standalone: true,
  imports: [
    MatStepperModule,
    ReactiveFormsModule,
    MatButton,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    DishSelectMenuComponent,
    JsonPipe,
  ],
  template: `
    <mat-vertical-stepper linear="true" #stepper>
      <mat-step [stepControl]="formMenuEntreeGroup">
        <ng-template matStepLabel>Entrée</ng-template>
        <form [formGroup]="formMenuEntreeGroup">
          <div class="">
          <mat-label>Entrée</mat-label>
          <app-dish-select-menu
            [dishList]="entreList"
            (idDishOutPut)="setEntreeID($event)"
          />
          </div>
          <input type="hidden" formControlName="entreeId" />
        </form>
        <button
          class="primary"
          [disabled]="!formMenuEntreeGroup.valid"
          mat-button
          matStepperNext
        >
          Choix du plat
        </button>
      </mat-step>

      <mat-step
      class="test"
        [stepControl]="formMenuMainDishGroup"
        errorMessage="Plat requis"
      >
        <ng-template matStepLabel>Plat</ng-template>
        <form [formGroup]="formMenuMainDishGroup">
          <mat-label>Plat</mat-label>
          <app-dish-select-menu
            [dishList]="mainDishList"
            (idDishOutPut)="setMainDishID($event)"
          />
          <input type="hidden" formControlName="mainDishId" />
        </form>
        <button mat-button matStepperPrevious>Choix de l'entrée</button>
        <button mat-button matStepperNext>Choix du dessert</button>
      </mat-step>

      <mat-step
        [stepControl]="formMenuMainDishGroup"
        errorMessage="Dessert requis"
      >
        <ng-template matStepLabel>Desert</ng-template>
        <form [formGroup]="formMenuDessertGroup">
          <mat-label>Desert</mat-label>
          <app-dish-select-menu
            [dishList]="desertDishList"
            (idDishOutPut)="setDessertID($event)"
          />
          <input type="hidden" formControlName="dessertId" />
        </form>
      </mat-step>

      <mat-step>
        <ng-template matStepLabel>Titre</ng-template>
        <form [formGroup]="formMenuGroup" (ngSubmit)="onSubmit()">
          <mat-form-field>
            <mat-label>Titre</mat-label>
            <input
              type="text"
              matInput
              formControlName="description"
              placeholder="Ex. Bœuf bourguignon"
            />
          </mat-form-field>

          <mat-form-field>
            <mat-label>Prix € entré/plat, plat dessert</mat-label>
            <input
              type="number"
              matInput
              formControlName="partielPrice"
              placeholder="Ex. 18"
            />
          </mat-form-field>

          <mat-form-field>
            <mat-label>Prix € menu (entré + plat + dessert)</mat-label>
            <input
              type="number"
              matInput
              formControlName="totalPrice"
              placeholder="Ex. 25"
            />
          </mat-form-field>

          <button
            [disabled]="!formMenuGroup.valid"
            type="submit"
            mat-raised-button
            color="primary"
          >
            Valider
          </button>
        </form>
      </mat-step>
    </mat-vertical-stepper>
  `,
  styles: `
  @import 'button';
   form {
     width: 100% !important;
     max-width: 100% !important;
   }

  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormMenuComponent implements OnInit {
  readonly store = inject(Store);
  onSubmit() {
    const menuCookerToPost: MenuCookerToPost = this.formMenuGroup
      .value as unknown as MenuCookerToPost;
    this.outIsAddingMenu.emit(false);
    this.store.dispatch(addMenuCooker({ newMenu: menuCookerToPost }));
  }
  DishType = DishType;
  dishList = input.required<Dish[]>();
  entreList: Dish[] = [];
  mainDishList: Dish[] = [];
  desertDishList: Dish[] = [];
  outIsAddingMenu = output<boolean>();

  formMenuGroup = new FormGroup({
    entreeId: new FormControl(-1, [Validators.min(1), Validators.required]),
    mainDishId: new FormControl(-1, [Validators.min(1), Validators.required]),
    dessertId: new FormControl(-1, [Validators.min(1), Validators.required]),
    description: new FormControl('', [Validators.required]),
    partielPrice: new FormControl('', [Validators.required]),
    totalPrice: new FormControl('', [Validators.required]),
  });

  formMenuEntreeGroup = new FormGroup({
    entreeId: new FormControl(-1, [Validators.min(1), Validators.required]),
  });

  formMenuMainDishGroup = new FormGroup({
    mainDishId: new FormControl(-1, [Validators.min(1), Validators.required]),
  });

  formMenuDessertGroup = new FormGroup({
    dessertId: new FormControl(-1, [Validators.min(1), Validators.required]),
  });

  formMenuCommonGroup = new FormGroup({
    description: new FormControl('', [Validators.required]),
    partielPrice: new FormControl('', [Validators.required]),
    totalPrice: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {
    this.entreList = this.dishList().filter(
      (dish: Dish) => dish.Type === DishType.Entree
    );
    this.mainDishList = this.dishList().filter(
      (dish: Dish) => dish.Type === DishType.MainDish
    );
    this.desertDishList = this.dishList().filter(
      (dish: Dish) => dish.Type === DishType.Dessert
    );
  }

  setEntreeID(id: number) {
    this.formMenuGroup.patchValue({
      entreeId: id,
    });
    this.formMenuEntreeGroup.patchValue({
      entreeId: id,
    });
  }

  setMainDishID(id: number) {
    this.formMenuGroup.patchValue({
      mainDishId: id,
    });
    this.formMenuMainDishGroup.patchValue({
      mainDishId: id,
    });
  }

  setDessertID(id: number) {
    this.formMenuGroup.patchValue({
      dessertId: id,
    });
    this.formMenuDessertGroup.patchValue({
      dessertId: id,
    });
  }
}
