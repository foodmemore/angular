import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
  input,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, RouterOutlet } from '@angular/router';
import { DishType } from 'app/core/dish/enum/dish-type.enum';
import { DishGatewayService } from 'app/core/dish/gateway/dish-gateway.service';
import { MatIconModule } from '@angular/material/icon';
import { Store } from '@ngrx/store';
import { addDishes } from 'app/core/dish/store/dish.action';
import { AsyncPipe } from '@angular/common';
import { Observable } from 'rxjs';
import { Allergen } from 'app/core/allergen/model/Allergen';
import { selectAllergenListState } from 'app/core/allergen/store/allergen.selector';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
  selector: 'app-form-dish',
  standalone: true,
  imports: [
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    RouterOutlet,
    RouterModule,
    AsyncPipe,
    MatCheckboxModule,
  ],
  template: `
    <form class="wrap" [formGroup]="dishForm" (ngSubmit)="onSubmit()">
      <!-- Nom -->
      <mat-form-field>
        <mat-label>Nom</mat-label>
        <input
          type="text"
          matInput
          formControlName="title"
          placeholder="Ex. Bœuf bourguignon"
        />
        @if(dishForm.get('title')?.hasError('required')) {
        <mat-error>Un nom est <strong>requis</strong></mat-error>
        } @else if(dishForm.get('title')?.hasError('minlength')) {
        <mat-error
          >Le nom doit avoir <strong>au moins 3 caractères</strong></mat-error
        >
        } @else if(dishForm.get('title')?.hasError('maxlength')) {
        <mat-error
          >Le nom doit avoir <strong>au plus 100 caractères</strong></mat-error
        >
        }
      </mat-form-field>
      <!-- Prix -->
      <mat-form-field class="price">
        <mat-label>Prix</mat-label>
        <input
          type="number"
          matInput
          formControlName="price"
          placeholder="Ex. 19.99"
          min="1"
          max="999"
          step="0.5"
        />
        <mat-icon matSuffix>euro</mat-icon>
        @if(dishForm.get('price')?.hasError('required')) {
        <mat-error>Un prix est <strong>requis</strong></mat-error>
        } @else if(dishForm.get('price')?.hasError('min')) {
        <mat-error>Le prix doit être <strong>au moins 1 €</strong></mat-error>
        } @else if(dishForm.get('price')?.hasError('max')) {
        <mat-error>Le prix doit être <strong>au plus 999 €</strong></mat-error>
        } @else if(dishForm.get('price')?.hasError('pattern')) {
        <mat-error>Le prix doit être <strong>un prix valide</strong></mat-error>
        }
      </mat-form-field>
      <!-- Description -->
      <mat-form-field class="example-full-width">
        <mat-label>Rentrez une description</mat-label>
        <textarea
          matInput
          rows="5"
          matAutosize
          formControlName="description"
          placeholder="Ex. Bœuf bourguignon mariné au vingt de bordeaux et sa salade"
        ></textarea>
        <mat-hint align="end"
          >{{ dishForm.get('description')?.value?.length }} /
          {{ maxDescriptionLength }}</mat-hint
        >
        @if(dishForm.get('description')?.hasError('maxlength')) {
        <mat-error
          >Le nom doit avoir
          <strong
            >au plus {{ maxDescriptionLength }} caractères</strong
          ></mat-error
        >
        }
      </mat-form-field>

      <input
        type="file"
        id="file"
        formControlName="file"
        (change)="onFileSelected($event)"
        class="file-input"
        accept="image/*"
      />
      <label for="file">
        <span class="material-icons">attach_file</span>
        @if(dishForm.get('file')?.value) {
        {{ dishForm.get('file')?.value.name }}
        } @else { Choisissez un fichier }
      </label>

      <!-- Allergens -->
      <h4>Sélectionnez les allergènes :</h4>
      @if(allergenList$ | async; as allergenList) { @for(allergen of
      allergenList; track $index) {
      <div class="allergen">
        <p>
          <mat-checkbox (click)="toggleAllergenID(allergen.ID)">
            {{ allergen.Libelle }}
          </mat-checkbox>
        </p>
        <p class="sm">{{ allergen.Description }}</p>
      </div>
      } }

      <button
        [disabled]="!dishForm.valid"
        class="primary"
        type="submit"
        mat-raised-button
      >
        Ajouter le plat
      </button>
    </form>
  `,
  styles: `
 @import "color";
 h4 {
  margin: 1rem 0;
 }
 .allergen {
  margin: .5rem 0;
 }
 .sm {
  padding-left: 27px;
 }
.file-input {
  display: none;

  & + label {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    padding: 0.5rem 1rem;
    font-size: 1rem;
    line-height: 1.25;
    border-radius: 4px;
    background-color: #fff;
    border: 1px solid #ced4da;
    cursor: pointer;
    transition: background-color 0.3s;

    &:hover {
      background-color: #e8e8e8;
    }

    .material-icons {
      margin-right: 8px;
    }
  }
}
form {
  display: flex;
  padding: 1rem 0;
  margin: 0 auto;
  max-width: 600px;

  .price {
    width: 200px;
    mat-icon {
      color: $secondFontColor;
      height: 30px;
      padding: 0;
      margin-right: 0.5rem;
      padding-top: 0.5rem;
    }
    input[type="number"] {
      width: 120px;
      -webkit-appearance: none;
      -moz-appearance: textfield;
    }
  }

  
}
button {
  display: inline-block;
  margin-top: 2rem;
  width: 200px;
}

  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormDishComponent implements OnInit {
  readonly store: Store = inject(Store);
  dishForm!: FormGroup;
  imageUrl: string | ArrayBuffer | null = '';
  dishGateway: DishGatewayService = inject(DishGatewayService);

  allergenList$: Observable<Allergen[]> = this.store.select(
    selectAllergenListState
  );

  maxDescriptionLength = 200;
  type = input.required<DishType>();
  ngOnInit(): void {
    this.dishForm = new FormGroup({
      type: new FormControl(this.type(), [Validators.required]),
      file: new FormControl('', [Validators.required]),
      price: new FormControl('', [
        Validators.required,
        Validators.min(0),
        Validators.max(999),
        Validators.pattern(/^\d+(\.\d{1,2})?$/),
      ]),
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100),
      ]),
      description: new FormControl('', [
        Validators.maxLength(this.maxDescriptionLength),
      ]),
      allergens: new FormControl([]),
    });
  }

  onSubmit() {
    if (!this.dishForm.valid) {
      return;
    }
    const newFormData = new FormData();
    newFormData.append('type', this.type()) ;
    newFormData.append('title', this.dishForm.get('title')?.value);
    newFormData.append('description', this.dishForm.get('description')?.value);
    newFormData.append('price', this.dishForm.get('price')?.value);
    newFormData.append('file', this.dishForm.get('file')?.value);
    newFormData.append('allergens', JSON.stringify(this.dishForm.get('allergens')?.value));
  

    this.addNewDish(newFormData);
  }

  onFileSelected(event: Event) {
    if (
      !event ||
      !(event.target instanceof HTMLInputElement) ||
      !event.target.files
    )
      return;
    if (!this.dishForm) return;

    if (this.dishForm && this.dishForm.get('file')) {
      const file = event.target.files[0];
      this.dishForm.get('file')?.setValue(file);

      // Créer l'aperçu de l'image
      const reader = new FileReader();
      reader.onload = () => {
        this.imageUrl = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  private addNewDish(dishFormData: FormData) {
    this.store.dispatch(addDishes({ dishFormData }));
  }

  toggleAllergenID(allergenId: number): void {
    const allergens = this.dishForm.get('allergens')?.value as number[];
  
    if (allergens.includes(allergenId)) {
      // L'allergène est déjà présent, on le retire
      const updatedAllergens = allergens.filter(id => id !== allergenId);
      this.dishForm.get('allergens')?.setValue(updatedAllergens);
    } else {
      // L'allergène n'est pas présent, on l'ajoute
      const updatedAllergens = [...allergens, allergenId];
      this.dishForm.get('allergens')?.setValue(updatedAllergens);

    }
  }
}
