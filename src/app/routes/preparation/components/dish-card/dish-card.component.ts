import { CurrencyPipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { Dish } from 'app/core/dish/model/dish';

@Component({
  selector: 'app-dish-card',
  standalone: true,
  imports: [JsonPipe, CurrencyPipe],
  template: `
    <div [class.inactive]="isInactive() && !isSelected()" [class.selected]="isSelected() && !isInactive()" class="card">
      <img src="{{ dish().ImgSrc }}" alt="{{ dish().Title }}" />
      <div class="text">
        <div class="content">
          <span class="title">{{ dish().Title }}</span>
          <span class="price">{{
            dish().Price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr'
          }}</span>

          <p class="description">{{ dish().Description }}</p>
        </div>
      </div>
    </div>
  `,
  styles: `
  @import 'color';
  @import 'card';

  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishCardComponent {
  dish = input.required<Dish>();
  isInactive = input<boolean>();
  isSelected = input<boolean>();
}
