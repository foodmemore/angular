import { Routes } from "@angular/router";
import { PreparationListRouteComponent } from "./preparation.component";
export const PREPARATION_ROUTES: Routes = [
    {
        path: '',
        component: PreparationListRouteComponent
    }
]