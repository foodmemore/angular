import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';

import { Menu } from 'app/core/menu/model/menu';
import { MenuCardComponent } from '../../card/menu-card/menu-card.component';
import { DialogAddMenuToBasketComponent } from '../../dialog/dialog-add-menu-to-basket/dialog-add-menu-to-basket.component';
import { MatDialog } from '@angular/material/dialog';
import { CurrencyPipe } from '@angular/common';
import { MatButton } from '@angular/material/button';
import { Store } from '@ngrx/store';

import { MenuGatewayService } from 'app/core/menu/gateway/menu.service';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';
import  * as MenuActions from 'app/core/menu/store/menu.action';

@Component({
  selector: 'app-menu-list-cooker',
  standalone: true,
  template: `
    <section class="card-list">
      @for(menu of menuList(); track menu) {
      <app-menu-card
        style="display: flex; justify-content: center"
        [menu]="menu"
      >
        <button
          type="button"
          mat-raised-button
          color="primary"
          (click)="toggleMenuAvailability(menu.ID)"
        >
          Changer disponibilité
        </button>
      </app-menu-card>

      }
    </section>
  `,
  styles: `
    @import "card";
    @import "button";
    @import "responsive";
    section {

      display: grid;
      grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
      gap: 20px;
    }
    @include breakpoints(bigTablet) {
      section {
        display: flex;
        flex-direction: column;
        align-items: center;
  
      }
    }
    `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [MenuCardComponent, CurrencyPipe, MatButton],
})
export class MenuListCookerComponent {
  menuList = input.required<Menu[]>();
  readonly dialog = inject(MatDialog);
  readonly store = inject(Store);
  readonly menuGatewayService = inject(MenuGatewayService);
  readonly snackBar = inject(SnackbarService);

  openDialog(menu: Menu) {
    this.dialog.open(DialogAddMenuToBasketComponent, {
      data: {
        menu: menu,
      },
      width: '100svw',
      maxWidth: '800px',
      maxHeight: '100svh',
    });
  }

  toggleMenuAvailability(id: number) {
    if (!id) return;
    this.store.dispatch(MenuActions.toggleMenuAvailability({ menuID: id }));
  }
}
