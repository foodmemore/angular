import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';

import { Menu } from 'app/core/menu/model/menu';
import { MenuCardComponent } from '../../card/menu-card/menu-card.component';
import { DialogAddMenuToBasketComponent } from '../../dialog/dialog-add-menu-to-basket/dialog-add-menu-to-basket.component';
import { MatDialog } from '@angular/material/dialog';
import { CurrencyPipe } from '@angular/common';
import { MatButton } from '@angular/material/button';
import { Store } from '@ngrx/store';

import { MenuGatewayService } from 'app/core/menu/gateway/menu.service';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';

@Component({
  selector: 'app-menu-list',
  standalone: true,
  template: `
    <section class="wrap">
      @for(menu of menuList(); track menu) { @if(menu.Open) {
      <app-menu-card
        style="display: flex; justify-content: center"
        [menu]="menu"
      >
        <button
          type="button"
          mat-raised-button
          color="primary"
          (click)="openDialog(menu)"
        >
          Ajouter au panier
        </button>
      </app-menu-card>
      } } @if(menuList().length === 0) {
      <button mat-raised-button color="primary">
        <a
          color="primary"
          target="_blank"
          href="https://195.35.25.202:4251/auth/ping"
        >
          Accepter le certifcat backend</a
        >
      </button>

      }
    </section>
  `,
  styles: `
  @import "wrap";
  @import "button";
  @import "responsive";
  section {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
    gap: 20px;
    margin-bottom: 3rem !important;
  }
  @include breakpoints(bigTablet) {
    section {
      display: flex;
      flex-direction: column;
      align-items: center;

    }
  }
  a {

  }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [MenuCardComponent, CurrencyPipe, MatButton],
})
export class MenuListComponent {
  menuList = input.required<Menu[]>();
  readonly dialog = inject(MatDialog);
  readonly store = inject(Store);
  readonly menuGatewayService = inject(MenuGatewayService);
  readonly snackBar = inject(SnackbarService);

  openDialog(menu: Menu) {
    this.dialog.open(DialogAddMenuToBasketComponent, {
      data: {
        menu: menu,
      },
      width: '100svw',
      maxWidth: '800px',
      maxHeight: '100svh',
    });
  }

  toggleMenuAvailability(id: number) {
    if (!id) return;
    // this.store.dispatch(MenuActions.toggleMenuAvailability({ menuID: id }));
    // const subscription: Subscription = this.menuGatewayService
    //   .toggleMenuAvailability(id)
    //   .subscribe({
    //     next: () => {
    // //      this.store.dispatch(MenuActions.getMenusCooker());
    //     },
    //     error: () => {
    //       this.snackBar.showSuccess(
    //         'Impossible de changer la disponibilité veuillez ressayer'
    //       );
    //     },
    //     complete: () => subscription.unsubscribe(),
    //   });
  }
}
