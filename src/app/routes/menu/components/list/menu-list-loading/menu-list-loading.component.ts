import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MenuCardLoadingComponent } from '../../card/menu-card-loading/menu-card-loading.component';

@Component({
  selector: 'app-menu-list-loading',
  standalone: true,
  template: `
    <section class="wrap">
      @for (item of items; track item.name) {
      <app-menu-card-loading />
      }
    </section>
  `,
  styles: `
  @import "wrap";
  @import "responsive";
  section {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
    gap: 20px;
  }
  @include breakpoints(bigTablet) {
    section {
      display: flex;
      flex-direction: column;
      align-items: center;

    }
  }  
    `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [MenuCardLoadingComponent],
})
export class MenuListLoadingComponent {
  items = Array.from({ length: 12 }, (_, i) => ({ name: `Item ${i}` }));
}
