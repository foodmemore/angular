import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuListLoadingComponent } from './menu-list-loading.component';

describe('MenuListLoadingComponent', () => {
  let component: MenuListLoadingComponent;
  let fixture: ComponentFixture<MenuListLoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MenuListLoadingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MenuListLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
