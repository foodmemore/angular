import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { MenuCommand } from 'app/core/command/model/command';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import * as CommandActions from './../../../../../core/command/store/command.action';
import { selectCurrentCommand } from 'app/core/command/store/command.selector';
import { AsyncPipe, JsonPipe } from '@angular/common';

@Component({
  selector: 'app-mat-form-command',
  standalone: true,
  imports: [
    MatButtonModule,
    MatStepperModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    AsyncPipe,
    JsonPipe,
  ],
  template: `
    <form [formGroup]="commandForm">
      <mat-form-field>
        <mat-label>Date du menu</mat-label>
        <input
          (dateChange)="updateCurrentDateCommand()"
          [min]="todayDate"
          matInput
          formControlName="date"
          [matDatepicker]="picker"
        />
        <mat-datepicker-toggle
          matIconSuffix
          [for]="picker"
        ></mat-datepicker-toggle>
        <mat-datepicker touchUi #picker></mat-datepicker>
      </mat-form-field>

      <div style="display: flex; justify-content: space-between">
        <mat-form-field>
          <mat-label>Heure</mat-label>
          <input
            matInput
            (change)="updateCurrentDateCommand()"
            formControlName="hour"
            [min]="10"
            [max]="19"
            [step]="1"
            type="number"
          />
        </mat-form-field>

        <mat-form-field>
          <mat-label>Minutes</mat-label>
          <input
            matInput
            (change)="updateCurrentDateCommand()"
            formControlName="minute"
            [min]="0"
            [max]="59"
            [step]="10"
            type="number"
          />
        </mat-form-field>
      </div>

      <mat-form-field>
        <mat-label>Nombre de personnes</mat-label>
        <input
          matInput
          (change)="updateCurrentCommand()"
          formControlName="userNumber"
          [min]="1"
          [max]="10"
          [step]="1"
          type="number"
        />
      </mat-form-field>

      <div>
        <button mat-button matStepperPrevious>Retour</button>
        <button
          type="button"
          color="primary"
          mat-raised-button
          [disabled]="commandForm.invalid"
          matStepperNext
          mat-raised-button
        >
          Commander
        </button>
      </div>
    </form>
  `,
  styles: `
  @import 'button';
  `,
  changeDetection: ChangeDetectionStrategy.Default,
})
export class CommandComponent implements OnInit, OnDestroy {
  todayDate: Date = new Date();
  store = inject(Store);
  currentCommand$ = this.store.select(selectCurrentCommand);

  commandForm: FormGroup = new FormGroup({
    date: new FormControl(this.todayDate),
    hour: new FormControl(10, [
      Validators.required,
      Validators.min(10),
      Validators.max(19),
    ]),
    minute: new FormControl(0, [
      Validators.required,
      Validators.min(0),
      Validators.max(59),
    ]),
    userNumber: new FormControl(1, [Validators.required, Validators.min(1)]),
  });

  updateCurrentCommand() {
    const { userNumber } = this.commandForm.value;
    const listCommand: MenuCommand[] = [];
    for (let i = 0; i < userNumber; i++) {
      listCommand.push({
        price: 0,
        entreeID: -1,
        mainDishID: -1,
        dessertID: -1,
      });
    }
    this.store.dispatch(
      CommandActions.setCommandMenu({
        menuCommands: listCommand,
      })
    );
  }

  updateCurrentDateCommand() {
    const { date, hour, minute } = this.commandForm.value;
    this.store.dispatch(CommandActions.setDateCommand({ date, hour, minute }));
  }

  ngOnInit(): void {
    this.updateCurrentCommand();
  }

  ngOnDestroy(): void {
    this.updateCurrentCommand();
    this.updateCurrentDateCommand();
  }
}
