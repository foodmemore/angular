import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
  input,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Store } from '@ngrx/store';
import { MenuCommand } from 'app/core/command/model/command';
import { Menu } from 'app/core/menu/model/menu';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { AsyncPipe, CurrencyPipe, JsonPipe } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import * as commandMenuActions from 'app/core/command/store/command.action';
@Component({
  selector: 'app-command-menu',
  standalone: true,
  imports: [
    FormsModule,
    CurrencyPipe,
    MatButtonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatStepperModule,
    MatCheckboxModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    AsyncPipe,
    JsonPipe,
  ],
  template: `
    <form [formGroup]="menuCommandForm">
      <h4>Choix des plats</h4>
      <p>
        <mat-checkbox (change)="updateCommand()" formControlName="isEntree"
          >Entrée : {{ menu().Entree.Title }}
          {{
            menu().Entree.Price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr'
          }}
        </mat-checkbox>
      </p>
      <p>
        <mat-checkbox (change)="updateCommand()" formControlName="isMainDish"
          >Plat : {{ menu().MainDish.Title }}
          {{
            menu().MainDish.Price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr'
          }}</mat-checkbox
        >
      </p>
      <p>
        <mat-checkbox (change)="updateCommand()" formControlName="isDessert"
          >Dessert : {{ menu().Dessert.Title }}
          {{
            menu().Dessert.Price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr'
          }}
        </mat-checkbox>
      </p>

      <div>
        <button mat-button matStepperPrevious>Retour</button>

        <button
          type="button"
          color="primary"
          (click)="updateCommand()"
          mat-raised-button
          [disabled]="menuCommandForm.invalid"
          matStepperNext
          mat-raised-button
        >
          Commander
        </button>
      </div>
    </form>
  `,
  styles: `
  @import 'button';
  form {
    margin: 12px 0;  
    p {
      margin: 0;
      padding: 1px 4px;
    }
  }
  `,
  changeDetection: ChangeDetectionStrategy.Default,
})
export class CommandMenuComponent implements OnInit {
  store = inject(Store);
  index = input.required<number>();
  menuCommand = input.required<MenuCommand>();
  menu = input.required<Menu>();
  menuCommandForm: FormGroup = new FormGroup(
    {
      isEntree: new FormControl(),
      isMainDish: new FormControl(),
      isDessert: new FormControl(),
    },
    { validators: this.menuCommandValidator() }
  );

  ngOnInit() {
    this.menuCommandForm.patchValue({
      isEntree: this.menuCommand().entreeID === -1 ? false : true,
      isMainDish: this.menuCommand().mainDishID === -1 ? false : true,
      isDessert: this.menuCommand().dessertID === -1 ? false : true,
    });
  }

  updateCommand() {
    let price = 0;

    const isEntree = this.menuCommandForm.value.isEntree;
    const isMainDish = this.menuCommandForm.value.isMainDish;
    const isDessert = this.menuCommandForm.value.isDessert;

    const isPartielPrice = isMainDish && (isDessert || isEntree);
    const isTotalPrice = isEntree && isMainDish && isDessert;
    const isEntreePrice = isEntree && !isMainDish && !isDessert;
    const isMainDishPrice = isMainDish && !isEntree && !isDessert;
    const isDesertPrice = isDessert && !isMainDish && !isEntree;
    const isEntreeAndDesertPrice = isEntree && !isMainDish && isDessert;

    if (isPartielPrice) {
      price = this.menu().PartialPrice;
    }
    if (isTotalPrice) {
      price = this.menu().TotalPrice;
    }
    if (isEntreePrice) {
      price = this.menu().Entree.Price;
    }
    if (isMainDishPrice) {
      price = this.menu().MainDish.Price;
    }
    if (isDesertPrice) {
      price = this.menu().Dessert.Price;
    }
    if (isEntreeAndDesertPrice) {
      price = this.menu().Entree.Price + this.menu().Dessert.Price;
    }

    const menuCommand: MenuCommand = {
      price: price,
      entreeID: this.menuCommandForm.value.isEntree
        ? this.menu().Entree.ID
        : -1,
      mainDishID: this.menuCommandForm.value.isMainDish
        ? this.menu().MainDish.ID
        : -1,
      dessertID: this.menuCommandForm.value.isDessert
        ? this.menu().Dessert.ID
        : -1,
    };

    this.store.dispatch(
      commandMenuActions.setNumberCommand({
        index: this.index(),
        menuCommand: menuCommand,
      })
    );
  }

  menuCommandValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const formGroup = control as FormGroup;
      let checked = false;

      ['isEntree', 'isMainDish', 'isDessert'].forEach((key) => {
        if (formGroup.get(key)?.value) {
          checked = true;
        }
      });

      return checked ? null : { notChecked: true };
    };
  }
}
