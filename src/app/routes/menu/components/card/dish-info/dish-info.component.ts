import { CurrencyPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { Dish } from 'app/core/dish/model/dish';

@Component({
  selector: 'app-dish-info',
  standalone: true,
  imports: [CurrencyPipe],
  template: `
    <h2>{{ dish().Title }}</h2>
    <p class="description">{{ dish().Description }}</p>
    <p class="price">{{ dish().Price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr' }}</p>
  `,
  styles: `
  @import 'card';
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishInfoComponent {
  dish = input.required<Dish>();
}
