import { ChangeDetectionStrategy, Component, output } from '@angular/core';
import { DishType } from 'app/core/dish/enum/dish-type.enum';

@Component({
  selector: 'app-dish-selector',
  standalone: true,
  imports: [],
  template: `
    <ul class="dishSelector">
      <li
        (click)="activeDishType(DishType.Entree)"
        (keydown)="onKeyDown($event, DishType.Entree)"
        tabindex="0"
        [class.active]="currentDish === DishType.Entree"
      >
        Entrée
      </li>
      <li
        (click)="activeDishType(DishType.MainDish)"
        (keydown)="onKeyDown($event, DishType.MainDish)"
        tabindex="0"
        [class.active]="currentDish === DishType.MainDish"
      >
        Plat
      </li>
      <li
        (click)="activeDishType(DishType.Dessert)"
        (keydown)="onKeyDown($event, DishType.Dessert)"
        tabindex="0"
        [class.active]="currentDish === DishType.Dessert"
      >
        Dessert
      </li>
    </ul>
  `,
  styles: `
  @import "color";
  .dishSelector {
  display: flex;
  justify-content: space-between;
  border-radius: 10px;
  margin-top: 10px;
  @include un-selectable();
  border: $border;
  li {
    padding: 5px;
    border-right: $border;
    cursor: pointer;
    font-size: 0.8rem;
    text-align: center;
    width: 70px;

    &:last-child {
      border-right: none;
    }
    &.active {
      font-weight: bold;
    }
  }
}
  
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishSelectorComponent {
  currentDishEmit = output<DishType>();
  onKeyDown(event: KeyboardEvent, dishType: DishType) {
    if (event.key === 'Enter' || event.key === ' ') {
      this.currentDish = dishType;
      this.currentDishEmit.emit(dishType);
      event.preventDefault();
    }
  }

  activeDishType(dishType: DishType) {
    this.currentDish = dishType;
    this.currentDishEmit.emit(dishType);
  }

  isLoading = false;

  DishType = DishType;
  currentDish: DishType = DishType.MainDish;
}
