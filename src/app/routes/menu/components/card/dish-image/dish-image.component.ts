import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { Dish } from 'app/core/dish/model/dish';

@Component({
  selector: 'app-dish-image',
  standalone: true,
  imports: [],
  template: `
    <img
      (load)="isLoading = false"
      loading="lazy"
   [src]="getTransformedImgSrc(dish().ImgSrc)"
      alt="{{ dish().Title }}"
    />
  `,
  styles: `
    img {
    width: 300px;
    height: 200px;
    border-radius: 10px;
    object-fit: cover;
    object-position: center;
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
  }
  
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishImageComponent {
  dish = input.required<Dish>();
  isLoading = true;

  getTransformedImgSrc(imgSrc: string): string {
    const transformation = 'q_auto,w_300,h_200';
    const parts = imgSrc.split('/upload/');
    return `${parts[0]}/upload/${transformation}/${parts[1]}`;
  }
}
