import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-menu-card-loading',
  standalone: true,
  imports: [],
  template: ` <div class="card loading"></div> `,
  styles: `
  .card {
    width: 300px;
    height: 500px;
    background-color: rgb(235, 235, 235);
    margin: 0.5rem;
  }

  .loading {

    background: #eee;
    background: linear-gradient(110deg, #ececec 8%, #f5f5f5 18%, #ececec 33%);
    border-radius: 5px;
    background-size: 200% 100%;
    animation: 1.5s shine linear infinite;
  }
  



  @keyframes shine {
  to {
    background-position-x: -200%;
  }
}
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuCardLoadingComponent {}
