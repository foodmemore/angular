
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCardLoadingComponent } from './menu-card-loading.component';

describe('MenuCardLoadingComponent', () => {
  let component: MenuCardLoadingComponent;
  let fixture: ComponentFixture<MenuCardLoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MenuCardLoadingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MenuCardLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
