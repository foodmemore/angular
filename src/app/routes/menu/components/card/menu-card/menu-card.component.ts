import { CurrencyPipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { Menu } from 'app/core/menu/model/menu';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { DishType } from 'app/core/dish/enum/dish-type.enum';
import { DishInfoComponent } from '../dish-info/dish-info.component';
import { DishSelectorComponent } from '../dish-selector/dish-selector.component';
import { DishImageComponent } from '../dish-image/dish-image.component';

@Component({
  selector: 'app-menu-card',
  standalone: true,
  template: `
    <div class="card menu" [class.inactive]="!menu().Open">
      @switch (currentDish) { @case (DishType.Entree) {
      <app-dish-image [dish]="menu().Entree" />
      } @case (DishType.MainDish) {
      <app-dish-image [dish]="menu().MainDish" />
      } @case (DishType.Dessert) {
      <app-dish-image [dish]="menu().Dessert" />
      } }

      <app-dish-selector (currentDishEmit)="currentDish = $event" />

      <div class="content">
        @switch (currentDish) { @case (DishType.Entree) {
        <app-dish-info [dish]="menu().Entree" />
        } @case (DishType.MainDish) {
        <app-dish-info [dish]="menu().MainDish" />
        } @case (DishType.Dessert) {
        <app-dish-info [dish]="menu().Dessert" />

        } }
      </div>

      <div class="btn-card-content">
        <ng-content #name> </ng-content>
      </div>
    </div>
  `,
  styles: `
@import 'card';

  
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    JsonPipe,
    MatButtonToggleModule,
    MatButtonModule,
    DishInfoComponent,
    DishSelectorComponent,
    DishImageComponent,
    CurrencyPipe,
  ],
})
export class MenuCardComponent {
  menu = input.required<Menu>();
  DishType = DishType;
  currentDish: DishType = DishType.MainDish;
}
