import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  inject,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { Menu } from 'app/core/menu/model/menu';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { AsyncPipe, CurrencyPipe, JsonPipe } from '@angular/common';
import { CommandComponent } from '../../form/command/command.component';
import { Store } from '@ngrx/store';
import { selectCurrentCommand } from 'app/core/command/store/command.selector';
import { CommandMenuComponent } from '../../form/command-menu/command-menu.component';
import {
  addCurrentCommand,
  setMenuIDCommand,
  toggleAnimation,
} from 'app/core/command/store/command.action';
import { Router } from '@angular/router';
import { DishDisplayComponent } from '../../../../basket/components/command-basket/dish-display/dish-display.component';

export interface DialogData {
  menu: Menu;
}

@Component({
  selector: 'app-dialog-add-menu-to-basket',
  standalone: true,
  template: `
    <mat-stepper orientation="vertical" [linear]="false" #stepper>
      <mat-step>
        <ng-template matStepLabel
          >Information Cuisinier | Menu :
          {{ data.menu.Description }}</ng-template
        >
        <h3>Menu</h3>
        <app-dish-display [dishID]="data.menu.Entree.ID" />
        <app-dish-display [dishID]="data.menu.MainDish.ID" />
        <app-dish-display [dishID]="data.menu.Dessert.ID" />
        <div>
          <button
            type="button"
            mat-raised-button
            color="primary"
            matStepperNext
            mat-raised-button
          >
            Commander
          </button>
        </div>
      </mat-step>

      <mat-step>
        <ng-template matStepLabel>Nombre de commande | Date</ng-template>
        <app-mat-form-command />
      </mat-step>

      @if(currentCommand$ | async; as currentCommand) { state @for (command of
      currentCommand.menuCommands; track command; let i = $index) {
      <mat-step>
        <ng-template matStepLabel>Menu #{{ i + 1 }}</ng-template>
        <app-command-menu
          [index]="i"
          [menu]="data.menu"
          [menuCommand]="command"
        />
      </mat-step>
      }

      <mat-step>
        <ng-template matStepLabel
          >Confirmation de la commande
          {{
            currentCommand.price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr'
          }}</ng-template
        >
        <div>
          <button mat-button matStepperPrevious>Retour</button>

          <button mat-button (click)="closeDialog()">
            Continuer mes achats
          </button>

          <button
            type="button"
            color="primary"
            (click)="goToCommand()"
            mat-raised-button
            matStepperNext
            mat-raised-button
          >
            Commander
          </button>
        </div>
      </mat-step>
      }
    </mat-stepper>
  `,
  styles: `
  @import 'button';
  

  
.mat-step-icon.mat-step-icon-state-number.mat-step-icon-selected {
	margin-right: 120px !important;
  background-color: red !important;
} 

  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatButtonModule,
    CurrencyPipe,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    JsonPipe,
    AsyncPipe,
    CommandComponent,
    CommandMenuComponent,
    DishDisplayComponent,
  ],
})
export class DialogAddMenuToBasketComponent implements OnInit {
  store = inject(Store);
  router = inject(Router);
  currentCommand$ = this.store.select(selectCurrentCommand);
  readonly _formBuilder = inject(FormBuilder);

  constructor(
    public dialogRef: MatDialogRef<DialogAddMenuToBasketComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {
    this.store.dispatch(setMenuIDCommand({ id: this.data.menu.ID }));
  }

  closeDialog() {
    this.store.dispatch(addCurrentCommand());
    setInterval(() => {
      this.store.dispatch(toggleAnimation({ changeActive: false }));
    }, 4000);
    this.dialogRef.close();
  }

  goToCommand() {
    this.router.navigate(['/panier']);
    this.closeDialog();
  }
}
