import { Component, inject, OnInit } from '@angular/core';
import { MenuListComponent } from "../components/list/menu-list/menu-list.component";
import { MenuListLoadingComponent } from "../components/list/menu-list-loading/menu-list-loading.component";
import { Store } from '@ngrx/store';
import { selectMenuListState } from 'app/core/menu/store/menu.selector';
import { AsyncPipe } from '@angular/common';
import { getMenus } from 'app/core/menu/store/menu.action';

@Component({
    selector: 'app-menu-route',
    standalone: true,
    template: `
@defer {
  @if(menuList$ | async; as menuList) {
    <app-menu-list [menuList]="menuList" />
  }

}
@placeholder (minimum 1ms) {
  <app-menu-list-loading />
}



  `,
    styles: ``,
    imports: [MenuListComponent, MenuListLoadingComponent, AsyncPipe]
})
export class MenuListRouteComponent implements OnInit {

  ngOnInit(): void {
    this.store.dispatch(getMenus());
  }

  readonly store: Store = inject(Store);
  menuList$ = this.store.select(selectMenuListState);


}
