import { Routes } from "@angular/router";
import { MenuListRouteComponent } from "./menu/menu.component";
export const MENU_ROUTES: Routes = [
    {
        path: '',
        component: MenuListRouteComponent
    }
]