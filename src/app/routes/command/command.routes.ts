import { Routes } from "@angular/router";
import { LayoutCommandComponent } from "./components/layout/layout-command/layout-command.component";
import { LayoutUserCommandComponent } from "./components/layout/layout-user-command/layout-user-command.component";
import { isCookerGuard } from "app/core/guard/is-cooker.guard";



export const COMMAND_ROUTES: Routes = [
    {
        path: '',
        canActivate: [isCookerGuard],
        component: LayoutCommandComponent
    },
    {
        path: 'utilisateur',
        component: LayoutUserCommandComponent
    }
]