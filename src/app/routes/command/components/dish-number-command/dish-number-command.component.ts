import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
} from '@angular/core';
import { NumberCount } from '../command-review/command-review.component';
import { JsonPipe } from '@angular/common';
import { DishDisplayComponent } from '../../../basket/components/command-basket/dish-display/dish-display.component';

@Component({
  selector: 'app-dish-number-command',
  standalone: true,
  imports: [JsonPipe, DishDisplayComponent],
  template: `
    @if(dishID() && dishNumber()) {

    <app-dish-display [dishID]="dishID()" [dishNumber]="dishNumber()" />
    }
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishNumberCommandComponent {
  commandNumber = input.required<NumberCount>();
  dishNumber = computed(() => Object.values(this.commandNumber())[0]);
  dishID = computed(() => parseInt(Object.keys(this.commandNumber())[0]));
}
