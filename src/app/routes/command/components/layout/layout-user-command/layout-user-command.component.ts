import { AsyncPipe, DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { CommandGatewayService } from 'app/core/command/gateway/command.gateway';
import { MenuCommandUserResponse } from 'app/core/command/model/command';
import { map, Observable } from 'rxjs';
import { DishDisplayComponent } from '../../../../basket/components/command-basket/dish-display/dish-display.component';

@Component({
  selector: 'app-layout-user-command',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe,
    DatePipe,
    MatListModule,
    MatDividerModule,
    DishDisplayComponent,
  ],
  template: `
    <section class="wrap">
      @if(menuCommandUserOrdered$ | async; as menuCommandUser) {

      <!-- {{menuCommandUser | json}} -->
      @for(menuCommandUser of menuCommandUser; track $index) {

      <h2>
        {{ menuCommandUser.DayToDeliver | date }} à
        {{ menuCommandUser.HourToDeliver }}:{{
          menuCommandUser.MinuteToDeliver > 10
            ? menuCommandUser.MinuteToDeliver
            : '00'
        }}
      </h2>

      <app-dish-display [dishID]="menuCommandUser.Menu.EntreeID" />
      <app-dish-display [dishID]="menuCommandUser.Menu.MainDishID" />
      <app-dish-display [dishID]="menuCommandUser.Menu.DessertID" />

      <!-- {{ menuCommandUser.Menu.CommandID | json }} -->

      <mat-divider></mat-divider>
      } }
    </section>
  `,
  styles: `
  @import "wrap";
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutUserCommandComponent {
  readonly menuCommandGateway = inject(CommandGatewayService);
  readonly menuCommandUser$: Observable<MenuCommandUserResponse[]> =
    this.menuCommandGateway.getMenuCommandUser();

  menuCommandUserOrdered$: Observable<MenuCommandUserResponse[]> =
    this.menuCommandUser$.pipe(
      map((menuCommandUser) =>
        menuCommandUser
          .sort(
            (a, b) =>
              new Date(a.DayToDeliver).getTime() -
              new Date(b.DayToDeliver).getTime()
          )
          .filter(
            (value, index, self) =>
              index ===
              self.findIndex(
                (t) =>
                  new Date(t.DayToDeliver).getDate() ===
                    new Date(value.DayToDeliver).getDate() &&
                  new Date(t.DayToDeliver).getMonth() ===
                    new Date(value.DayToDeliver).getMonth()
              )
          )
      )
    );
  menuCommandUser!: [];
}
