/* eslint-disable @typescript-eslint/no-explicit-any */
import { AsyncPipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommandGatewayService } from 'app/core/command/gateway/command.gateway';
import { MenuCommandResponse, MenuCommandResponseWellTyped } from 'app/core/command/model/command';
import { map, Observable } from 'rxjs';
import { CommandReviewComponent } from '../../command-review/command-review.component';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-layout-command',
  standalone: true,
  imports: [
    AsyncPipe,
    JsonPipe,
    CommandReviewComponent,
    MatListModule,
    MatDividerModule,
  ],
  template: `
    <p>
      @if(commandsCombinedDateAndMenuID$ | async; as commands){
      <mat-list>
        @for(command of commands; track $index) {

        <app-command-review [command]="command" />
       }
      </mat-list>
      }
    </p>
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutCommandComponent {
  readonly commandGateway = inject(CommandGatewayService);

  commands$: Observable<MenuCommandResponse[]> =
    this.commandGateway.getMenuCommandCooker();
  commandsOrdered$: Observable<MenuCommandResponse[]> = this.commands$.pipe(
    map((commands) =>
      commands.sort(
        (a, b) =>
          new Date(a.DayToDeliver).getTime() -
          new Date(b.DayToDeliver).getTime()
      )
    )
  );
  commandsCombinedDateAndMenuID$: Observable<MenuCommandResponseWellTyped[]> =
    this.commandsOrdered$.pipe(
      map((commands) => {
        const groupedCommands = commands.reduce((acc, command) => {
          // const dateKey = `${new Date(command.DayToDeliver).toDateString()}-${
          //   command.MenuID
          // }`;
          const dateKey = `${new Date(command.DayToDeliver).toDateString()}`
          if (!acc[dateKey]) {
            acc[dateKey] = {
              ...command,
              countNumber: 1,
              totalMenuPrice: 0,
              totalEntreNumber: [],
              totalMainDishNumber: [],
              totalDessertNumber: [],
            };
          } else {
            acc[dateKey].countNumber += 1;
            acc[dateKey].totalMenuPrice += command.Price;

            if (command.EntreeID !== -1) {
              acc[dateKey].totalEntreNumber.push(command.EntreeID);
            }
            if (command.MainDishID !== -1) {
              acc[dateKey].totalMainDishNumber.push(command.MainDishID);
            }
            if (command.DessertID !== -1) {
              acc[dateKey].totalDessertNumber.push(command.DessertID);
            }
          }
          return acc;
        }, {} as { [key: string]: any });

        return Object.values(groupedCommands);
      })
    );
}
