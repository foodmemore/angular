import { CurrencyPipe, DatePipe, JsonPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  input,
  OnInit,
  signal,
} from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MenuCommandResponseWellTyped } from 'app/core/command/model/command';
import { DishNumberCommandComponent } from "../dish-number-command/dish-number-command.component";
export interface NumberCount {
  [key: number]: number;
}

@Component({
  selector: 'app-command-review',
  standalone: true,
  imports: [MatExpansionModule, JsonPipe, CurrencyPipe, DatePipe, DishNumberCommandComponent],
  template: `
    <section class="wrap">
      <mat-accordion>
        <mat-expansion-panel
          (opened)="panelOpenState.set(true)"
          (closed)="panelOpenState.set(false)"
        >
          <mat-expansion-panel-header>
            <mat-panel-title
              >nombre de commande : {{ command().countNumber }}
             </mat-panel-title
            >
            <mat-panel-description>
              {{ command().Menu.Description }} |
              {{ command().DayToDeliver | date : 'dd/MM/yyyy' }} à

              {{ command().HourToDeliver }}:{{
                command().MinuteToDeliver > 10
                  ? command().MinuteToDeliver
                  : '00'
              }}
            </mat-panel-description>
          </mat-expansion-panel-header>

          <div>Total : {{ command().totalMenuPrice | currency : 'EUR' }}</div>
          @if(command().totalEntreNumber.length > 0) {
          <strong>Liste des entrées ({{ command().totalEntreNumber.length }}) :</strong>
          @for(idDishNumber of entreeNumber; track $index){
          <app-dish-number-command [commandNumber]="idDishNumber" />
          } 
        } 
        @if(command().totalMainDishNumber.length > 0) {
          <strong>Liste des plats ({{ command().totalMainDishNumber.length }}) :</strong>
          @for(idDishNumber of mainDishNumber; track $index){
          <app-dish-number-command [commandNumber]="idDishNumber" />
          } 
        } 
        @if(command().totalDessertNumber.length > 0) {
          <strong>Liste des desserts ({{ command().totalDessertNumber.length }}) :</strong>
          @for(idDishNumber of dessertNumber; track $index){
          <app-dish-number-command [commandNumber]="idDishNumber" />
          } 
        } 
        </mat-expansion-panel>
      </mat-accordion>
    </section>
  `,
  styles: `
  @import "wrap";
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommandReviewComponent implements OnInit {
  command = input.required<MenuCommandResponseWellTyped>();
  readonly panelOpenState = signal(false);
  entreeNumber: NumberCount[] = [];
  mainDishNumber: NumberCount[] = [];
  dessertNumber: NumberCount[] = [];
  // eNumber: number = this.command()?.totalEntreNumber ? this.command().totalEntreNumber.length : 0;

  transformArray(arr: number[]): NumberCount[] {
    const countMap = arr.reduce((acc, num) => {
      acc.set(num, (acc.get(num) || 0) + 1);
      return acc;
    }, new Map<number, number>());

    const result: NumberCount[] = Array.from(countMap).map(([key, value]) => ({
      [key]: value,
    }));

    return result;
  }

  ngOnInit(): void {
    this.entreeNumber = this.transformArray(
      this.command().totalEntreNumber as number[]
    );
    this.mainDishNumber = this.transformArray(
      this.command().totalMainDishNumber as number[]
    );
    this.dessertNumber = this.transformArray(
      this.command().totalDessertNumber as number[]
    );

  }
}
