import { Routes } from "@angular/router";
import { SettingProfileComponent } from "./components/setting/setting.component";

export const PROFILE_ROUTES: Routes = [
    {
        path: '',
        component: SettingProfileComponent
    }
]