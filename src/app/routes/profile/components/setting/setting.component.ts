import { ChangeDetectionStrategy, Component, OnInit, inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Store } from '@ngrx/store';
import * as UserActions from './../../../../core/user/store/user.action';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { selectCurrentUserState } from 'app/core/user/store/user.selector';
import { AsyncPipe, JsonPipe } from '@angular/common';
import { UserCurrentUseRole } from 'app/core/user/store/user.state';
import { User } from 'app/core/user/model/user.model';
import { updateUserUseRole } from 'app/core/user/store/user.action';
import { Observable, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogCguComponent } from 'app/routes/condition/dialog/dialog-cgu/dialog-cgu.component';
import { DialogConfidentialityComponent } from 'app/routes/condition/dialog/dialog/dialog-confidentiality/dialog-confidentiality.component';
import { UserGatewayService } from 'app/core/user/gateway/user.gateway';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';

@Component({
  selector: 'app-setting-profile',
  standalone: true,
  imports: [
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    AsyncPipe,
    JsonPipe,
  ],
  template: `
    <section class="wrap">
      <mat-list>
        <!-- <section class="content">
          <div class="wrap">
            <h2>Profile</h2>
            <button mat-raised-button>Changer de mot de passe</button>
            <button mat-raised-button>Changer d'image de profile</button>
          </div>
        </section> -->

        <!-- <mat-divider></mat-divider> -->

        @if(userState$ | async; as user) {
        <section class="content">
          @if(isCooker(user)) {
          <div class="wrap">
            <h2>Cuisinier</h2>

            @if(isCurrentRoleUser(user)) { 
            <button (click)="setUseRole(UserCurrentUseRole.COOKER)" mat-raised-button>Devenir Cuisinier</button>
            } @else {
              <button (click)="setUseRole(UserCurrentUseRole.USER)" mat-raised-button>Devenir Utilisateur</button>
            }
          </div>

          } @else {
          <div class="wrap">
            <h2>Devenir Cuisinier</h2>
            <button (click)="askingToBecomeCooker()" mat-raised-button>Demande</button>
          </div>

          }
        </section>

        }

        <mat-divider></mat-divider>

        <section class="content">
          <div class="wrap">
            <h2>Option</h2>
            <button (click)="openConfidentiality()" mat-raised-button>Politique de confidentialité</button>
            <button (click)="openCgu()" mat-raised-button>CGU</button>
            <button (click)="logout()" mat-raised-button>Se déconnecter</button>
          </div>
        </section>
      </mat-list>
    </section>
  `,
  styles: `@import "wrap";
  .content {
    padding: 10px 0;
    .wrap {
      display: flex;
      flex-flow: column wrap;
      button {
        margin: 10px 0;
        width: 250px;
      }
      align-items: start;
    }
  }
  
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingProfileComponent implements OnInit {
  readonly store = inject(Store);
  readonly userGateway = inject(UserGatewayService);
  readonly snackBar = inject(SnackbarService);
  userState$!: Observable<User>;
  UserCurrentUseRole = UserCurrentUseRole;
  readonly dialog = inject(MatDialog);
  openCgu() {
    this.dialog.open(DialogCguComponent);
  }
  openConfidentiality() {
    this.dialog.open(DialogConfidentialityComponent);
  }

  ngOnInit(): void {
    this.userState$ = this.store.select(selectCurrentUserState);
  }

  logout() {
    this.store.dispatch(UserActions.logout());
  }

  isCooker(user: User) {
    return user.roles.includes(UserCurrentUseRole.COOKER);
  }

  isCurrentRoleUser(user: User) {
    return user.currentUseRole === UserCurrentUseRole.USER;
  }

  setUseRole(role: UserCurrentUseRole) {
    this.store.dispatch(updateUserUseRole( {useCurrentRole: role}))
   
  }

  askingToBecomeCooker() {
    const sub: Subscription = this.userGateway.askToBecomeCooker().subscribe({
      next: () => this.snackBar.showSuccess('Demande envoyée'),
      error: () => this.snackBar.showError('Impossible d\'envoyer la demande'),
      complete: () => sub.unsubscribe()
    })
  }
}
