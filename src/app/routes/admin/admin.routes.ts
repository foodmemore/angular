import { Routes } from "@angular/router";
import { SettingAdminComponent } from "./components/setting/setting.component";

export const ADMIN_ROUTES: Routes = [
    {
        path: '',
        component: SettingAdminComponent
    }
]