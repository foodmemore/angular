import { AsyncPipe, JsonPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  inject,
} from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';
import { UserGatewayService, UserLicenseRequest } from 'app/core/user/gateway/user.gateway';
import { Subscription } from 'rxjs';


export interface UserResponse {
  ID: number;
  FirstName: string;
  LastName: string;
  Roles: string[];
  Email: string;
  IsAskingToBeCook: boolean;
}

@Component({
  selector: 'app-setting-admin',
  standalone: true,
  imports: [MatTableModule, AsyncPipe, JsonPipe, MatButton, MatFormFieldModule],
  template: `
    <section class="wrap">
      @if(users$ | async; as users) {
1

      <table mat-table [dataSource]="users" class="mat-elevation-z8">

        <ng-container matColumnDef="ID">
          <th mat-header-cell *matHeaderCellDef>ID</th>
          <td mat-cell *matCellDef="let element">{{ element.ID }}</td>
        </ng-container>


        <ng-container matColumnDef="Name">
          <th mat-header-cell *matHeaderCellDef>Name</th>
          <td mat-cell *matCellDef="let element">
            {{ element.FirstName }} {{ element.LastName }}
          </td>
        </ng-container>

    
        <ng-container matColumnDef="Roles">
          <th mat-header-cell *matHeaderCellDef>Roles</th>
          <td mat-cell *matCellDef="let element">{{ element.Roles }}</td>
        </ng-container>

      
        <ng-container matColumnDef="Email">
          <th mat-header-cell *matHeaderCellDef>Email</th>
          <td mat-cell *matCellDef="let element">{{ element.Email }}</td>
        </ng-container>

        <ng-container matColumnDef="IsAskingToBeCook">
          <th mat-header-cell *matHeaderCellDef>Demande à être cuisinier</th>
          <td mat-cell *matCellDef="let element">
            @if(element.IsAskingToBeCook ) {
            <button (click)="setValidCooker(element.ID)" mat-button color="primary">Valider la demande</button>

            }
          </td>
        </ng-container>

        <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
      </table>
      }
    </section>
  `,
  styles: `
  section {
    padding: 1rem;
    margin-bottom: 3rem;
  }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingAdminComponent {
  displayedColumns: string[] = [
    'ID',
    'Name',
    'Roles',
    'Email',
    'IsAskingToBeCook',
  ];

  private userGateWay = inject(UserGatewayService);
  private snackBar = inject(SnackbarService);

  users$ = this.userGateWay.getAll();

  setValidCooker(id: number) {
    const userLicenseRequest: UserLicenseRequest = {
      accept: true,
      user_id: id,
    }

    const sub: Subscription = this.userGateWay.adminAdeptUserBecomeCooker(userLicenseRequest).subscribe({
      next: () => { 
        this.snackBar.showSuccess('Cuisinier valide')
      },
      error: () => { 
        this.snackBar.showError('Impossible de valider le cuisinier')
      },
      complete: () => sub.unsubscribe()

    });
  }
}
