import { Component } from '@angular/core';
import { LoginComponent } from '../../../core/auth/form/login/login.component';

@Component({
  selector: 'app-login',
  standalone: true,
  template: `
    <section class="center">
      <h1>Connexion</h1>
      <app-form-login />
    </section>
  `,
  styles: `
  @import 'wrap';
  section {
    padding-bottom: 10vh;
    h1 {
        margin-bottom: 2rem;
    }

  }
  `,
  imports: [LoginComponent],
})
export class LoginRouteComponent {}
