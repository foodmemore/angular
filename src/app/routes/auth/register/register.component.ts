import { Component } from '@angular/core';
import { RegisterComponent } from '../../../core/auth/form/register/register.component';

@Component({
  selector: 'app-register',
  standalone: true,
  template: `
    <section class="center">
      <h1>Inscription</h1>
      <app-form-register />
    </section>
  `,
  styles: `
  @import 'wrap';
  section {
    padding-bottom: 10vh;
    h1 {
        margin-bottom: 2rem;
    }

  }
  `,
  imports: [RegisterComponent],
})
export class RegisterRouteComponent {}
