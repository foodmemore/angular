// eslint-disable-next-line @angular-eslint/template/click-events-have-key-events
import { JsonPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  inject,
  output,
  signal,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatStepperModule } from '@angular/material/stepper';
import {
  AuthenticationGatewayService,
  RegisterEmailPassword,
  RegisterFinalUpdate,
  RegisterToken,
} from 'app/core/auth/gateway/gateway.service';
import { SnackbarService } from 'app/core/helpers/snackbar/snackbar.service';
import { DialogCguComponent } from 'app/routes/condition/dialog/dialog-cgu/dialog-cgu.component';
import { DialogConfidentialityComponent } from 'app/routes/condition/dialog/dialog/dialog-confidentiality/dialog-confidentiality.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form-stepper-register',
  standalone: true,
  imports: [
    MatStepperModule,
    ReactiveFormsModule,
    MatButton,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatCheckboxModule,
    JsonPipe,
  ],
  template: `
    <mat-stepper orientation="vertical" [linear]="true" #stepper>
      <mat-step [stepControl]="firstFormGroup" [editable]="false">
        <form [formGroup]="firstFormGroup">
          <ng-template matStepLabel>Email, mots de passe</ng-template>
          <mat-form-field>
            <mat-label>Email</mat-label>
            <input
              matInput
              formControlName="email"
              placeholder="Email"
              required
            />
          </mat-form-field>
          @if(firstFormGroup.get('email')?.hasError('email') &&
          firstFormGroup.get('email')?.touched) {
          <mat-error>Veuillez entrer une adresse email valide</mat-error>
          } @if(firstFormGroup.get('email')?.hasError('required') &&
          firstFormGroup.get('email')?.touched) {
          <mat-error>L'adresse email est <strong>requise</strong></mat-error>
          }
          <mat-form-field>
            <mat-label>Password</mat-label>
            <input
              matInput
              formControlName="password"
              type="password"
              placeholder="Password"
              required
            />
          </mat-form-field>
          @if(firstFormGroup.get('password')?.hasError('required') &&
          firstFormGroup.get('password')?.touched) {
          <mat-error>Mot de passe <strong>requis</strong></mat-error>
          }
          <span
            ><mat-checkbox formControlName="cgu">
              Je certifie avoir lu et accepté les CGU
            </mat-checkbox>
          </span>

          <span
            class="checkbox-text"
            tabindex="0"
            (click)="openCgu()"
            (keyup.enter)="openCgu()"
          >
            Voir ici
          </span>

          <span>
            <mat-checkbox formControlName="confidently"
              >Je certifie avoir lu et accepté la politique de
              confidentialité</mat-checkbox
            >
          </span>
          <span
            class="checkbox-text"
            class="checkbox-text"
            tabindex="0"
            (keyup.enter)="openCgu()"
            (click)="openConfidentiality()"
          >
            Voir ici</span
          >
          <div>
            @if(!isMailValid()) {
            <button
              type="button"
              mat-raised-button
              [disabled]="!firstFormGroup.valid"
              color="primary"
              (click)="verifyEmail()"
            >
              Vérification Email
            </button>
            } @else {
            <button
              type="button"
              mat-raised-button
              matStepperNext
              color="primary"
            >
              Suivant
            </button>
            }
          </div>
        </form>
      </mat-step>

      <mat-step [stepControl]="secondFormGroup" [editable]="isEditable"
        ><form [formGroup]="secondFormGroup">
          <ng-template matStepLabel>Vérification du token</ng-template>

          <mat-form-field>
            <input
              matInput
              [disabled]="isTokenInvalid()"
              formControlName="token"
              placeholder="Token"
              required
            />
          </mat-form-field>
          @if(isTokenInvalid()) {
          <mat-error>Token invalide</mat-error>
          } @if(secondFormGroup.get('token')?.hasError('required') &&
          secondFormGroup.get('token')?.touched) {
          <mat-error>Token <strong>requis</strong></mat-error>
          }
          <div>
            @if(!isTokenValid() && !isTokenInvalid()) {
            <button
              type="button"
              mat-raised-button
              [disabled]="!secondFormGroup.valid"
              color="primary"
              (click)="verifyToken()"
            >
              Vérification Token
            </button>
            } @else if (isTokenInvalid()) {
            <button
              type="button"
              mat-raised-button
              color="primary"
              (click)="sendToken()"
            >
              Renvoyer un token
            </button>
            } @else if (isTokenValid()) {
            <button
              type="button"
              mat-raised-button
              matStepperNext
              color="primary"
            >
              Suivant
            </button>
            }
          </div>
        </form>
      </mat-step>

      <mat-step [stepControl]="thirdFormGroup" [editable]="isEditable">
        <ng-template matStepLabel>Information de utilisateur</ng-template>
        <form [formGroup]="thirdFormGroup">
          <mat-form-field>
            <input
              matInput
              formControlName="first_name"
              placeholder="Prénom"
              required
            />
          </mat-form-field>

          <mat-form-field>
            <input
              matInput
              formControlName="last_name"
              placeholder="Nom"
              required
            />
          </mat-form-field>
          <mat-form-field>
            <input
              matInput
              formControlName="address_street"
              placeholder="Street Address"
              required
            />
          </mat-form-field>
          <mat-form-field>
            <input
              matInput
              formControlName="address_city"
              placeholder="City"
              required
            />
          </mat-form-field>
          <mat-form-field>
            <input
              matInput
              formControlName="address_zip"
              placeholder="ZIP Code"
              required
            />
          </mat-form-field>
          @if(!isFinalFormValid()) {
          <button
            type="button"
            mat-raised-button
            [disabled]="!thirdFormGroup.valid"
            color="primary"
            (click)="finalForm()"
          >
            Mise des information de l'utilisateur
          </button>
          } @else {
          <button
            type="button"
            mat-raised-button
            matStepperNext
            color="primary"
            (click)="emitEmail()"
          >
            Inscription terminer
          </button>
          }
        </form>
      </mat-step>

      <mat-step>
        <ng-template matStepLabel>Inscription terminé</ng-template>
        <p>Bravo ! Inscription réussi</p>
        <div>
          <button
            type="button"
            mat-raised-button
            matStepperNext
            color="primary"
            (click)="goToIsLoggedIn.emit(true)"
          >
            Se connecter
          </button>
        </div>
      </mat-step>
    </mat-stepper>
  `,
  styles: `
  @import 'button';
  @import 'color';
    .hide {
    display: none;
    visibility: hidden;
  }
  .checkbox-text {

    color: $primaryColorLightHover;
    font-size: 14px;
    display: flex;
    align-items: center;
  
    padding-top: 0.5rem;
    padding-bottom: 1rem;
    padding-left: 25px;
    cursor: pointer;
  }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormStepperRegisterComponent {
  readonly dialog = inject(MatDialog);
  openCgu() {
    this.dialog.open(DialogCguComponent);
  }
  openConfidentiality() {
    this.dialog.open(DialogConfidentialityComponent);
  }
  authGateWay = inject(AuthenticationGatewayService);
  goToIsLoggedIn = output<boolean>();
  email = output<string>();
  snackBar = inject(SnackbarService);
  firstFormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
    cgu: new FormControl(false, [Validators.requiredTrue]),
    confidently: new FormControl(false, [Validators.requiredTrue]),
  });
  secondFormGroup = new FormGroup({
    token: new FormControl('', Validators.required),
  });

  thirdFormGroup = new FormGroup({
    first_name: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
    ]),
    last_name: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
    ]),
    address_street: new FormControl('', Validators.required),
    address_city: new FormControl('', Validators.required),
    address_zip: new FormControl('', Validators.required),
  });

  isEditable = false;

  isMailValid = signal(false);
  isTokenValid = signal(false);
  isTokenInvalid = signal(false);
  isFinalFormValid = signal(false);

  verifyEmail() {
    if (!this.firstFormGroup.valid) return;
    const registerEmailPassword: RegisterEmailPassword = {
      email: this.firstFormGroup.get('email')?.value as string,
      password: this.firstFormGroup.get('password')?.value as string,
    };

    const sub: Subscription = this.authGateWay
      .registerVerifyMail(registerEmailPassword)
      .subscribe({
        next: () => {
          this.isMailValid.set(true);
          this.snackBar.showSuccess(
            'Un mail de confirmation vous a été envoyé'
          );
        },
        error: () => this.snackBar.showError('Une erreur est survenue'),
        complete: () => sub.unsubscribe(),
      });
  }

  verifyToken() {
    if (!this.secondFormGroup.valid) return;
    const registerToken: RegisterToken = {
      email: this.firstFormGroup.get('email')?.value as string,
      password: this.firstFormGroup.get('password')?.value as string,
      token: this.secondFormGroup.get('token')?.value as string,
    };

    const sub: Subscription = this.authGateWay
      .registerVerifyToken(registerToken)
      .subscribe({
        next: () => {
          this.snackBar.showSuccess('Token valide'),
            this.isTokenValid.set(true);
        },
        error: () => {
          this.isTokenValid.set(false),
            this.isTokenInvalid.set(true),
            this.secondFormGroup.get('token')?.setValue('');
        },
        complete: () => sub.unsubscribe(),
      });
  }

  finalForm() {
    // if (!this.thirdFormGroup.valid) return;
    const registerFinalUpdate: RegisterFinalUpdate = {
      email: this.firstFormGroup.get('email')?.value as string,
      password: this.firstFormGroup.get('password')?.value as string,
      first_name: this.thirdFormGroup.get('first_name')?.value as string,
      last_name: this.thirdFormGroup.get('last_name')?.value as string,
      address_street: this.thirdFormGroup.get('address_street')
        ?.value as string,
      address_city: this.thirdFormGroup.get('address_city')?.value as string,
      address_zip_code: this.thirdFormGroup.get('address_zip')?.value as string,
    };
    const sub: Subscription = this.authGateWay
      .registerFinalUpdate(registerFinalUpdate)
      .subscribe({
        next: () => {
          this.isFinalFormValid.set(true),
          this.snackBar.showSuccess('Inscription validee');
        },
        error: () => this.isFinalFormValid.set(false),
        complete: () => sub.unsubscribe(),
      });
  }

  sendToken() {
    const email: string = this.firstFormGroup.get('email')?.value as string;
    const sub: Subscription = this.authGateWay.sendToken(email).subscribe({
      next: () => {
        this.isTokenInvalid.set(false),
          this.isTokenValid.set(false),
          this.snackBar.showSuccess(
            'Un mail de confirmation vous a été envoyé'
          );
      },
      error: () => this.snackBar.showError('Une erreur est survenue'),
      complete: () => sub.unsubscribe(),
    });
  }

  emitEmail() {
    const email: string = this.firstFormGroup.get('email')?.value as string;
    this.email.emit(email);
  }
}
