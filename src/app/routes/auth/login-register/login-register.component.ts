import { ChangeDetectionStrategy, Component, Input, signal } from '@angular/core';
import { LoginComponent } from '../../../core/auth/form/login/login.component';
import { JsonPipe } from '@angular/common';
import { RegisterComponent } from '../../../core/auth/form/register/register.component';
import { FormStepperRegisterComponent } from '../register/components/form-stepper-register/form-stepper-register.component';

@Component({
  selector: 'app-login-register',
  standalone: true,
  template: `
    <section class="wrap">
      @if(isLogging) {
      <h1>Connexion</h1>

      <app-form-login [emailInput]="email()">
        <p style="margin-top: 1rem" class="sm">
          Pas de compte ?           <a
            tabindex="0"
            (click)="toggleIsLogging()"
            (keydown.enter)="toggleIsLogging()"
            >Inscrivez-vous !</a
          >
        </p>
      </app-form-login>
      } @else {
      <h1>Inscription</h1>

      <app-form-stepper-register (email)="getEmail($event)" (goToIsLoggedIn)="toggleIsLogging()">

      <p style="margin-top: 1rem" class="sm">
          Déjà inscrit ?
          <a
            tabindex="0"
            (click)="toggleIsLogging()"
            (keydown.enter)="toggleIsLogging()"
            >Connectez-vous !</a
          >
        </p>
      </app-form-stepper-register>
              }
    </section>
  `,
  styles: `
    @import 'wrap';
    @import 'color';
  section {
    padding-bottom: 10vh;
    display: flex;
    flex-flow: column wrap;
    justify-content: center;
    align-items: center;
    h1 {
        margin-top: 2rem;
        margin-bottom: 2rem;
    }
    a {
      color: $primaryColorLight !important;
    }

  }
  
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    LoginComponent,
    JsonPipe,
    RegisterComponent,
    FormStepperRegisterComponent,
  ],
})
export class LoginRegisterComponent {
  email = signal<string>('');
  @Input() isLogging: boolean = true;
  // isLogging = signal(this.isLoggingInput())
  toggleIsLogging() {
    this.isLogging = !this.isLogging;
  }

  getEmail(email: string) {
    this.email.set(email);
  }
}
