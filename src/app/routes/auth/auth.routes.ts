import { Routes } from '@angular/router';
import { LoginRegisterComponent } from './login-register/login-register.component';
export const AUTH_ROUTES: Routes = [
  {
    path: 'connexion',
    component: LoginRegisterComponent,
  },
];
