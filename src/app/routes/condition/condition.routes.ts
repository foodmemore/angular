import { Routes } from "@angular/router";
import { CguComponent } from "./components/cgu/cgu.component";
import { ConfidentialityComponent } from "./components/confidentiality/confidentiality.component";


export const CONDITION_ROUTES: Routes = [
    {
        path: 'confidentialite',
        component: ConfidentialityComponent
    },
    {
        path: 'cgu',
        component: CguComponent
    }
]