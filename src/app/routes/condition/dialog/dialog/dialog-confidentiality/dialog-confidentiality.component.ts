import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ConfidentialityComponent } from '../../../components/confidentiality/confidentiality.component';
import {
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle,
} from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'app-dialog-confidentiality',
  standalone: true,
  imports: [
    ConfidentialityComponent,
    MatDialogActions,
    MatDialogClose,
    MatDialogContent,
    MatButton,
    MatDialogTitle,
  ],
  template: `
    <h2 mat-dialog-title>Condition générale d'utilisation</h2>
    <mat-dialog-content>
      <app-confidentiality></app-confidentiality>
      <mat-dialog-actions>
        <button
          color="primary"
          mat-raised-button
          mat-dialog-close
          cdkFocusInitial
        >
          Lu
        </button>
      </mat-dialog-actions>
    </mat-dialog-content>
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogConfidentialityComponent {
  readonly dialogRef = inject(MatDialogRef<DialogConfidentialityComponent>);

  onNoClick(): void {
    this.dialogRef.close();
  }
}
