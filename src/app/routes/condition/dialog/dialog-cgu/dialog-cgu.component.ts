import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import {
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle,
} from '@angular/material/dialog';
import { ConfidentialityComponent } from '../../components/confidentiality/confidentiality.component';
import { CguComponent } from '../../components/cgu/cgu.component';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'app-dialog-cgu',
  standalone: true,
  imports: [
    MatButton,
    MatDialogActions,
    MatDialogClose,
    MatDialogContent,
    MatDialogTitle,
    ConfidentialityComponent,
    CguComponent,
  ],
  template: `
    <h2 mat-dialog-title>Condition générale d'utilisation</h2>
    <mat-dialog-content>
      <app-cgu></app-cgu>
      <mat-dialog-actions>
        <button color="primary" mat-raised-button mat-dialog-close cdkFocusInitial>Lu</button>
      </mat-dialog-actions>
    </mat-dialog-content>
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogCguComponent {
  readonly dialogRef = inject(MatDialogRef<DialogCguComponent>);

  onNoClick(): void {
    this.dialogRef.close();
  }
}
