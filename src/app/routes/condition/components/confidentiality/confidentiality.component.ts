import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-confidentiality',
  standalone: true,
  imports: [],
  template: `
  <section class="wrap">
    <h1>Politique de Confidentialité</h1>
    <p>
      Bienvenue sur le site FoodMeMore. En utilisant notre site, vous acceptez
      les présentes conditions de notre politique de confidentialité. Veuillez
      les lire attentivement.
    </p>

    <h2>1. Absence de Collecte de Données Personnelles</h2>
    <p>
      FoodMeMore ne collecte aucune donnée personnelle. Notre site est destiné
      uniquement à un usage scolaire et ne nécessite pas la collecte, le
      stockage ou le traitement de vos informations personnelles.
    </p>

    <h2>2. Utilisation des Cookies</h2>
    <p>
      FoodMeMore utilise des cookies uniquement pour faciliter la connexion et
      améliorer l'expérience utilisateur. Aucune information personnelle n'est
      stockée ou partagée via ces cookies.
    </p>

    <h2>3. Sécurité</h2>
    <p>
      Bien que nous ne collections pas de données personnelles, nous nous
      engageons à maintenir un niveau de sécurité élevé pour protéger les
      informations que vous pourriez échanger avec notre site, comme des
      requêtes ou des préférences de navigation.
    </p>

    <h2>4. Modifications de la Politique de Confidentialité</h2>
    <p>
      FoodMeMore se réserve le droit de modifier cette politique de
      confidentialité à tout moment. Les modifications prendront effet dès leur
      publication sur le site. Il vous est conseillé de consulter régulièrement
      cette page pour prendre connaissance des éventuelles modifications.
    </p>

    <h2>5. Contact</h2>
    <p>
      Si vous avez des questions concernant cette politique de confidentialité,
      veuillez nous contacter à l'adresse suivante :
    </p>
    <p>
      Adresse : 10 Rue du Général Sarrail, 76000 Rouen<br />
      Téléphone : 02 35 70 04 04
    </p>

    <p>Merci d'utiliser FoodMeMore.</p>
    </section>
  `,
  styles: `@import "wrap";
  section {
    margin-top: 2rem !important;
  }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfidentialityComponent {}
