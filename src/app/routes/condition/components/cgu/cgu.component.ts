import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-cgu',
  standalone: true,
  imports: [],
  template: `
    <section class="wrap">
      <h1>Conditions Générales d'Utilisation</h1>
      <p>
        Bienvenue sur le site FoodMeMore. En utilisant notre site, vous acceptez
        les présentes conditions générales d'utilisation (CGU). Veuillez les
        lire attentivement.
      </p>

      <h2>1. Acceptation des CGU</h2>
      <p>
        En accédant et en utilisant le site FoodMeMore, vous acceptez de vous
        conformer à ces CGU. Si vous n'acceptez pas ces termes, veuillez ne pas
        utiliser notre site.
      </p>

      <h2>2. Utilisation du Site</h2>
      <p>
        Vous vous engagez à utiliser le site de manière légale et à respecter
        toutes les lois et réglementations en vigueur. Vous acceptez de ne pas
        utiliser le site pour :
      </p>
      <ul>
        <li>
          Transmettre des contenus illégaux, nuisibles, menaçants, abusifs,
          harcelants, diffamatoires, vulgaires, obscènes ou autrement
          répréhensibles.
        </li>
        <li>
          Violer les droits de propriété intellectuelle de FoodMeMore ou de
          tiers.
        </li>
        <li>
          Propager des virus ou d'autres codes informatiques malveillants.
        </li>
      </ul>

      <h2>3. Cookies</h2>
      <p>
        FoodMeMore utilise des cookies pour améliorer votre expérience de
        navigation. En utilisant notre site, vous acceptez l'utilisation de
        cookies conformément à notre politique de cookies.
      </p>

      <h2>4. Protection des Données Personnelles</h2>
      <p>
        Vos données personnelles sont collectées et traitées conformément à
        notre politique de confidentialité. Veuillez la consulter pour plus
        d'informations sur la manière dont nous gérons vos données.
      </p>

      <h2>5. Modifications des CGU</h2>
      <p>
        FoodMeMore se réserve le droit de modifier ces CGU à tout moment. Les
        modifications prendront effet dès leur publication sur le site. Il vous
        est conseillé de consulter régulièrement cette page pour prendre
        connaissance des éventuelles modifications.
      </p>

      <h2>6. Contact</h2>
      <p>
        Si vous avez des questions concernant ces CGU, veuillez nous contacter à
        l'adresse suivante :
      </p>
      <p>
        Adresse : 10 Rue du Général Sarrail, 76000 Rouen<br />
        Téléphone : 02 35 70 04 04
      </p>

      <p>Merci d'utiliser FoodMeMore.</p>
    </section>
  `,
  styles: `@import "wrap";
  section {
    margin-top: 2rem !important;
  }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CguComponent {}
