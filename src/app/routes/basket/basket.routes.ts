import { Routes } from "@angular/router";
import { LayoutBasketComponent } from "./components/layout/layout.component";


export const BASKET_ROUTES: Routes = [
    {
        path: '',
        component: LayoutBasketComponent
    }
]