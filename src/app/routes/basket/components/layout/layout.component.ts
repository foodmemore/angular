import { AsyncPipe, CurrencyPipe, JsonPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  inject,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { selectCommands } from 'app/core/command/store/command.selector';
import { CommandBasketComponent } from '../command-basket/command-basket.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { commandRequest } from 'app/core/command/model/command';
import { map, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { CheckoutFormComponent } from 'app/shared/dialog/dialog-stripe/dialog-stripe.component';
import { PaymentGatewayService } from 'app/core/payment/gateway/payment.gateway';
import { selectCurrentUserState } from 'app/core/user/store/user.selector';
import { User } from 'app/core/user/model/user.model';
import { UserCurrentUseRole } from 'app/core/user/store/user.state';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-layout-basket',
  standalone: true,
  template: `
    @if(user$ | async; as user) {
    <section class="wrap padding-top">
      <h1>Mon panier :</h1>

      @if(commands$ | async; as commands) { @if(commands.length === 0) {
      <p>Votre panier est vide</p>
      } @else { @for(command of commands; track command; let index = $index) {
      <app-command-basket [tabIndex]="index" [command]="command" />
      } @if(isUserLoggedIn) {
      <button
        (click)="openDialog(user, commands)"
        color="primary"
        mat-raised-button
      >
        Payer
        {{
          totalCommandsPrice$
            | async
            | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr'
        }}
      </button>
      } @if(!isUserLoggedIn) {
      <div class="info">
        <p>Vous devez être connecte pour payer</p>
        <button color="primary" mat-raised-button [routerLink]="['/connexion']">
          Se connecter
        </button>
      </div>

      } } } @else {
      <p>Chargement</p>
      }


    </section>
    }
  `,
  styles: `
  @import 'wrap.scss';
  @import 'button.scss';
  section {
    h1 {
      margin-bottom: 1rem;
    }
    padding-top: 1rem;
    button {
      margin: 1rem;
    }
  }
  .info {
    margin: 1rem ;
  }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterModule,
    CurrencyPipe,
    AsyncPipe,
    MatExpansionModule,
    CommandBasketComponent,
    MatButtonModule,
    JsonPipe,
  ],
})
export class LayoutBasketComponent implements OnInit {
  ngOnInit(): void {
    const subscription: Subscription = this.isUserLoggedIn$.subscribe({
      next: (isUserLoggedIn) => (this.isUserLoggedIn = isUserLoggedIn),
      complete: () => subscription.unsubscribe(),
    });

  }
  readonly dialog = inject(MatDialog);
  readonly paymentGatewayService = inject(PaymentGatewayService);
  store: Store = inject(Store);
  commands$ = this.store.select(selectCommands);

  totalCommandsPrice$ = this.commands$.pipe(
    map((commands: commandRequest[]) => {
      return commands
        .map((command) => command.price)
        .reduce((a: number, b: number) => a + b, 0);
    })
  );
  UserCurrentUseRole = UserCurrentUseRole;

  user$ = this.store.select(selectCurrentUserState);
  isUserLoggedIn$ = this.user$.pipe(
    map((user: User) => user.currentUseRole !== UserCurrentUseRole.NOT_LOGGED)
  );
  isUserLoggedIn!: boolean;

  async openDialog(user: User, commands: commandRequest[]) {
    const subscription2: Subscription = this.totalCommandsPrice$.subscribe(
      (totalPrice) => {
        if (totalPrice <= 0) return;
        const roundedNumber = Math.ceil(totalPrice * 100);
        const subscription: Subscription = this.paymentGatewayService
          .createStripePaymentIntent(roundedNumber * 100)
          .subscribe({
            next: (response) => {
              this.dialog.open(CheckoutFormComponent, {
                data: {
                  clientSecret: response.clientSecret as string,
                  user,
                  commands,
                },
                width: '100svw',
                maxWidth: '600px',
              });
            },
            error: (error) => {
              console.error(error);
            },
            complete: () => {
              subscription.unsubscribe();
              subscription2.unsubscribe();
            },
          });
      }
    );
  }
}
