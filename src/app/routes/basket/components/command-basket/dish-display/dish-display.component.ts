import { AsyncPipe, CurrencyPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
  OnInit,
} from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { DishType } from 'app/core/dish/enum/dish-type.enum';
import { DishGatewayService } from 'app/core/dish/gateway/dish-gateway.service';
import { Dish } from 'app/core/dish/model/dish';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dish-display',
  standalone: true,
  imports: [AsyncPipe, CurrencyPipe, MatExpansionModule],
  template: `
    @if(dish$ | async; as dish) {
    <div class="dish">
      <img
        loading="lazy"
        width="180"
        height="120"
        src="{{ getTransformedImgSrc(dish.ImgSrc) }}"
        alt="{{ dish.Title }}"
      />
      <div class="content">
        <mat-accordion>
          <mat-expansion-panel>
            <mat-expansion-panel-header>
              {{ dish.Title }}

              <em
                >{{ dish.Price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr' }}
              </em>

              <mat-panel-description>
                @if(dishNumber()) { Nombre commandé : {{ dishNumber() }}
                }@else { @switch (dish.Type) { @case (DishType.Entree) {
                <mat-panel-title>Entré </mat-panel-title>
                } @case (DishType.MainDish) {
                <mat-panel-title>Plat </mat-panel-title>
                } @case (DishType.Dessert) {
                <mat-panel-title>Dessert</mat-panel-title>
                } } }
              </mat-panel-description>
            </mat-expansion-panel-header>
            <p>{{ dish.Description }}</p>
          </mat-expansion-panel>

          @if(!dishNumber()) { @if(dish.Allergens.length > 0) {
          <mat-expansion-panel>
            <mat-expansion-panel-header>
              <mat-panel-title style="width: 100%;">
                Se plat contient {{ dish.Allergens.length }} allergènes
              </mat-panel-title>
            </mat-expansion-panel-header>
            <p>Liste des allergènes.</p>
            @for(allergen of dish.Allergens; track $index) {
            <p>{{ $index + 1 }}. {{ allergen.Libelle }}</p>
            <p class="sm">{{ allergen.Description }}</p>
            }
          </mat-expansion-panel>
          } @else {
          <mat-expansion-panel disabled>
            <mat-expansion-panel-header>
              <mat-panel-title style="width: 100%; color: black"
                >Se plat ne contient aucun allergène</mat-panel-title
              >
              <!-- <mat-panel-description> </mat-panel-description> -->
            </mat-expansion-panel-header>
          </mat-expansion-panel>
          } }
        </mat-accordion>
      </div>
    </div>
    }
  `,
  styles: `
  @import "responsive";
    img {
    width: 180px;
    height: 120px;
    border-radius: 10px;
    object-fit: cover;
    object-position: center;
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
  }
  em {
    padding: 0 5px;
  }
  .dish {
    display: flex;
    gap: 10px;
    padding: 10px;
    .content {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      width: 100%;
    }
  }
  @include breakpoints(tinyTablet) {
    img {
      display: none;
    }
  }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DishDisplayComponent implements OnInit {
  dishGateway = inject(DishGatewayService);
  dishID = input<number>();
  dishNumber = input<number>();
  dish$!: Observable<Dish>;
  DishType = DishType;

  ngOnInit(): void {
    if (this.dishID()) {
      this.dish$ = this.dishGateway.getOne(
        this.dishID() as number
      ) as Observable<Dish>;
    }
  }

  getTransformedImgSrc(imgSrc: string): string {
    const transformation = 'q_auto,w_180,h_120';
    const parts = imgSrc.split('/upload/');
    return `${parts[0]}/upload/${transformation}/${parts[1]}`;
  }
}
