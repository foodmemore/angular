import { CurrencyPipe, DatePipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  input,
  signal,
} from '@angular/core';
import { commandRequest } from 'app/core/command/model/command';
import {
  MatExpansionModule,
  MatExpansionPanel,
} from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DishDisplayComponent } from './dish-display/dish-display.component';
@Component({
  selector: 'app-command-basket',
  standalone: true,
  viewProviders: [MatExpansionPanel],
  template: `
    <mat-accordion>
      <mat-expansion-panel [expanded]="false" hideToggle="true">
        <mat-expansion-panel-header>
          <mat-panel-title>
            Commande #{{ tabIndex() + 1 }} :
            {{ command().price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr' }}
          </mat-panel-title>
          <mat-panel-description
            >Le
            {{ command().date | date : 'dd/MM/yyyy' }}</mat-panel-description
          >
        </mat-expansion-panel-header>

        <div>
          @for(menuCommand of command().menuCommands; track command; let index =
          $index) {
          <mat-list-item>
            <h3>
              Menu #{{ index + 1 }} prix :
              {{
                menuCommand.price | currency : 'EUR' : 'symbol' : '1.2-2' : 'fr'
              }}
            </h3>
            @if (menuCommand.entreeID !== -1) {
            <app-dish-display [dishID]="menuCommand.entreeID" />
            } @if (menuCommand.mainDishID !== -1) {
            <app-dish-display [dishID]="menuCommand.mainDishID" />
            } @if (menuCommand.dessertID !== -1) {
            <app-dish-display [dishID]="menuCommand.dessertID" />
            }
          </mat-list-item>
          <mat-divider></mat-divider>
          }
        </div>
      </mat-expansion-panel>
    </mat-accordion>


  `,
  styles: `
    .red {
      background-color: red;
      min-height: 50px;
    }
    `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatExpansionModule,
    MatIconModule,
    MatListModule,
    DatePipe,
    CurrencyPipe,
    DishDisplayComponent,
  ],
})
export class CommandBasketComponent {
  command = input.required<commandRequest>();
  tabIndex = input.required<number>();
  readonly panelOpenState = signal(false);
}
