import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-layout-message',
  standalone: true,
  imports: [],
  template: `
    <p>
      layout works!
    </p>
  `,
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutMessageComponent {

}
