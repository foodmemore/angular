import { Routes } from "@angular/router";
import { LayoutMessageComponent } from "./components/layout/layout.component";

export const MESSAGES_ROUTES: Routes = [
    {
        path: '',
        component: LayoutMessageComponent
    }
]