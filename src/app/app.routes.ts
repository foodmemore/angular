import { Routes } from '@angular/router';
import { isLoggedGuard } from './core/guard/is-logged.guard';
import { isCookerGuard } from './core/guard/is-cooker.guard';
import { isAdminGuard } from './core/guard/is-admin.guard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full',
  },

  {
    path: 'menu',
    loadChildren: () =>
      import('./routes/menu/menu.routes').then((r) => r.MENU_ROUTES),
  },

  {
    path: 'preparation',
    loadChildren: () =>
      import('./routes/preparation/preparation.routes').then(
        (r) => r.PREPARATION_ROUTES
      ),
    canActivateChild: [isLoggedGuard, isCookerGuard],
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./routes/profile/profile.routes').then((r) => r.PROFILE_ROUTES),
    canActivateChild: [isLoggedGuard],
  },
  {
    path: 'panier',
    loadChildren: () =>
      import('./routes/basket/basket.routes').then((r) => r.BASKET_ROUTES),
  },
  {
    path: 'commande',
    loadChildren: () =>
      import('./routes/command/command.routes').then((r) => r.COMMAND_ROUTES),
    canActivateChild: [isLoggedGuard],
  },

  {
    path: 'condition',
    loadChildren: () =>
      import('./routes/condition/condition.routes').then(
        (r) => r.CONDITION_ROUTES
      ),
  },

  {
    path: 'message',
    loadChildren: () =>
      import('./routes/message/message.routes').then((r) => r.MESSAGES_ROUTES),
    canActivateChild: [isLoggedGuard],
  },

  {
    path: 'admin',
    loadChildren: () =>
      import('./routes/admin/admin.routes').then((r) => r.ADMIN_ROUTES),
    canActivate: [isAdminGuard],
  },

  {
    path: '',
    loadChildren: () =>
      import('./routes/auth/auth.routes').then((r) => r.AUTH_ROUTES),
  },
  {
    path: '**',
    redirectTo: 'menu',
    pathMatch: 'full',
  },
];
