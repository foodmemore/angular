# Stage 1: Build using the production config
FROM node:latest as build
# Step 1: Set up the working directory
WORKDIR /app
# Step 2: Copy the package.json and package-lock.json files
COPY package*.json ./
# Step 3: Install the dependencies (clean install)
RUN npm ci
# Step 4: Install Angular CLI
RUN npm install -g @angular/cli
# Step 5: Copy the rest of the application code
COPY . .
# Step 6: Build the application
RUN npm run build --configuration=production

# Stage 2: Serve using the production config
FROM nginx:latest
RUN mkdir -p /etc/letsencrypt/live/maxence-bossin.eu
# Step 1: Set up the working directory
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist/angular/browser /usr/share/nginx/html
 # Step 2: Expose the port
EXPOSE 80
EXPOSE 443

# build : docker build -t angular .
# run : docker run -p 8080:80 angular